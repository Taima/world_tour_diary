# Guan Yunchang’s World Tour Diary

Welcome to the smelly poo zone! I’m not very good at this diary thing, or this internet thing, but I’ll be trying to improve as the diary entries go on.

For information on the MapleStory area-locked challenge that my character Yunchang is partaking in, take a look at the [**World Tour Challenge Resources**](https://codeberg.org/Taima/world_tour). Yes, I peek at [**deer’s**](https://deer.codeberg.page/) formatting for pointers the same way I copied homework in school.

I play on **MapleLegends**, an even smellier, poopier zone than this page you’re reading now.
