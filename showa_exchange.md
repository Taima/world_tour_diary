## Showa Exchange Results

Total Equips Obtained: 2

-   Pink Bandana
    -   8 Wdef
    -   3 Speed
-   Pink Bandana
    -   8 Wdef
    -   3 Speed

### 100 Raccoon Firewood

Total attempts: 4

-   5 White Potion
-   15 Takoyaki (Octopus Ball)
-   20 Blue Potion
-   Mana Elixir

### 100 Crow Feather

Total attempts: 1

-   Mana Elixir

### 100 Cloud Foxtail

Total attempts: 13

-   10 Blue Potion
-   20 Blue Potion x4
-   10 White Potion
-   20 White Potion x2
-   Pink Bandana x2
-   Mana Elixir x2
-   15 Takoyaki (Octopus Ball)
