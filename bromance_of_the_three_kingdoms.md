## The Bromance of the Three Kingdoms

This is going to get confusing, so let’s do one of these cast listing things, like for a play I guess? The names begin with the family name, then their personal name, followed by their style name in parentheses. Women don’t get style names, apparently. If I’m doing this thing wrong or in an offensive manner, let me know! This bastardization of the Three Kingdoms characters and plot is definitely not the worst of what’s out there, I assure you. I’m probably doing a ton of shit wrong, but I’m having fun at least.

Characters not yet featured in the story will have illustrations for their pictures (or whatever image I can find), but I’ll swap those out for character pictures when they are later revealed. Kinda like unlocking a new character in a fighting game or something?

Since these are historical figures and are featured in one of the [great classical novels](https://en.wikipedia.org/wiki/Classic_Chinese_Novels) of Chinese literature, I’d like to stress that this is a parody and all in good fun. I come from a region where nobody knows what a Three Kingdoms even is! Honestly I can’t fucking explain this Three Kingdoms thing, so I’ll just hope literally a single person is able to understand what’s going on.

---

**Shu Han and the Brothers Liu**

Rather fine folks that serve under Liu Bei to restore the Han Dynasty. Eventually Shu Han becomes an empire under him, but we try not to talk about that, sort of ruins the story.

![The Brotherhood of the Mushroom Garden](./chapters/002/images/52_the_brotherhood_of_the_mushroom_garden.png)

*From Left to Right*

-   Guan Yu (Yunchang): Our main man, who has somehow become a girl, and you’re just gonna deal with it. Why would anyone play a male character on MapleStory anyway? For some reason they always depict Guan Yu with red skin all over like a high school football coach. Has a sense of honor as strong and heavy as iron.
-   Liu Bei (Xuande): The star of the show (except for Guan Yu); the man’s got big earlobes and an even bigger sense of filial piety. A scion of the Han, who would do anything to protect the people.
-   Zhang Fei (Yide): The youngest brother, often depicted as a powerful, disheveled, bearded man. Really likes drinking, and wields an epic serpent spear.

![This Is Also Guan Yu](./chapters/002/images/53_this_is_also_guan_yu.png)

![Sleeping Dragon](./chapters/003/images/075_sleeping_dragon.png)

-   Zhuge Liang (Kongming): A famed strategist known as Sleeping Dragon. Liu Bei travels thrice to his residence, meeting him on the third visit, thus forever gaining Zhuge’s trust and admiration. Smarter than me, and smarter than you.

There are more of these Shu Han guys, I just can’t figure out how to write them in yet. Give me some time, okay?

---

**Sun Wu**

Irrelevant bastards, whenever you reach a scene with these guys just skip it honestly.

![Sun Quan](./chapters/002/images/54_sun_quan.png)

-   Sun Quan (Zhongmou): Baby boy Sun, forced to come to power after his father and big brother died. How did they die? Idk, haven’t written it yet.

![Sun Ce](./chapters/002/images/55_sun_ce.png)

-   Sun Ce (Bofu): Bigger boy Sun, known as the Little McConqueror after he took a bunch of land or some shit. He probably smokes weed or surfs.

![Sun Jian](./chapters/002/images/56_sun_jian.png)

-   Sun Jian (Wentai): Big daddy Sun. The Tiger of Jiangdong. Over 99 billion served, and counting. He got done dirty early in the story by dying after he got the Imperial Seal, and his sons kind of suck, don’t they?

![Zhou Yu](./chapters/002/images/57_zhou_yu.png)

-   Zhou Yu (Gongjin): The big tactical man hiding up the Sun clan’s sleeve. He really fucking hates Zhuge Liang because Zhou Yu is clearly dumber and can’t stop proving just how easy he is to outsmart. Good as a second banana tactician, but he would never allow that. Smokes weed or surfs in his spare time, and microdoses estrogen.

![Lü Meng](./chapters/002/images/58_lu_meng.png)

-   Lü Meng (Ziming): A general guy or something under Sun Whateverhisnameis. Starts off kinda incompetent, but studies up later on and does a little bit of kickass military strategy. Probably used to smoke weed or surf with Sun Ce, and totally smokes weed or surfs with Zhou Yu.

![Da Qiao](./chapters/002/images/59_da_qiao.png)

-   Da Qiao: Of the Two Qiao Sisters, she is the Older Qiao. These two sisters are supposed to be so pretty they’re famous, a feat that has never been accomplished before or since in the history of humanity. Married to Sun Ce, but that won’t stop her.

![Xiao Qiao](./chapters/002/images/60_xiao_qiao.png)

-   Xiao Qiao: The Younger Qiao. Married to Zhou Yu, and he hasn’t shut up about her since. Zhuge Liang once suggested to Zhou Yu that he hand both sisters to Cao Cao and surrender, and Zhou Yu still flicks his bean to the thought to this day.

---

**Cao Wei**

The Microsoft of the Three Kingdoms era – complete with a policy of [embrace, extend, and extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish). They basically win in the end, which explains all Chinese history from there on out. You can try to surrender, but if Cao Cao wants your blood, he’s going to get it anyway.

![Cao Cao](./chapters/002/images/61_cao_cao.png)

-   Cao Cao (Mengde): TSAO TSAO, not COW COW! Just telling you before he gets angry. Cao Cao will do anything for power, and he thrives in this chaotic era. He’s pretty smart, but lets his love for Guan Yu get in the way sometimes. Holds ambition deep in his heart like a minimum wage shift leader.

Don’t worry, more of the Cao guys are coming.

---

**Other**

Besides the actual Three Kingdoms people, there are some other relevantly irrelevant fuckers who sometimes pop up in the story.

![The Love Triangle](./chapters/003/images/076_the_love_triangle.png)

*From left to right*

-   Lü Bu (Fengxian): They try to make this guy the main character, but he’s too neurodivergent in a useless way, and basically is like a teenager mentally. Despite being a fully grown adult, keeps asking older men to adopt him because he MURDERED ALL OF HIS PAST ADOPTED FATHERS!!!!! Who the fuck would agree to adopt this guy? Basically his one and only talent bestowed upon him by Heaven is being really, really strong (and he's got his girl Diao Chan, but she’s just playing both sides to come out on top). He's got cute antennae at least. His neo-daddy Dong Zhuo was a piece of shit, so good on Lü Bu for killing that fuck. Why didn’t Lü Bu have to change his family name to Dong? It would have been a little funny. He also had a cool and SUPER FAST red horse called Red Hare (hare like a bunny, but it also literally has red hair too sometimes), and as the saying goes: Among men, Lü Bu; Among steeds, Red Hare.

-   Dong Zhuo (Zhongying): To explain Lü Bu, you’ve gotta explain the fat sex offender behind him. He did a lot of bad shit, and I’m not sure how much is actually relevant to whatever this diary is. This dude seized control of Luoyang, the capital at the time, replaced the emperor with a younger more puppety emperor (still Han though), had an alliance form against him, burned up the capital and ran away to Chang’an which he then made the NEW capital which was spicy I guess, and was known to never once wash his stinking privates. He probably took Diao Chan’s innocence, which is the reason Lü Bu sliced him up into bacon.

-   Diao Chan: Daughter of Wang Yun, a guy with a funny family name who was an old school Han dynasty minister man. She’s almost as kawaii as she is manipulative, because she basically tore up the whole shit with Dong Zhuo’s reign on purpose by courting both Dong Zhuo and his adopted son, Lü Bu. Diao-chan successfully enough follows through with her father's plot to destroy the usurper. Did it save the Han dynasty? Uh...

![Red Hare, Not Looking Particularly Red](./chapters/002/images/62_red_hare_not_looking_particularly_red.png)

-   Red Hare: Yeah, it’s just a horse, but it’s going in the character cast list. Red Hare is taken by Cao Cao after Lü Bu does really dumb shit to get killed off, and Cao Cao later gifts it to Guan Yu. If it weren’t for Guan Yu, we wouldn’t be talking about this fucking horse. Where is Red Hare now, anyway?

![Yuan Shao](./chapters/002/images/63_yuan_shao.png)

-   Yuan Shao (Benchu): I’m not even going to dignify this guy with a proper entry.

![Cringe](./chapters/002/images/64_yuan_shu.png)

-   Yuan Shu (Gonglu): No.

---

Check this shit out! On this [shitty retail Maple$tory site](https://maplestory.nexon.net/news/20210/cash-shop-update-1-18) (under Lunar New Year Permanent Equipment Covers) there are official™ face and hairstyles for those Three Kingdoms character costumes featured in Maple! (Here's a link to the [actual full image](https://nxcache.nexon.net/umbraco/9744/lny782364r4.jpg) too.)

![Official Three Kingdoms Maple Characters!](./chapters/003/images/074_official_three_kingdoms_maple_characters.jpg)

I bet Diao Chan has Prudent Face, the best face in the game. Why didn’t they make a Lü Bu set though?

By the way, the last three characters on the right here are from [Journey to the West](https://en.wikipedia.org/wiki/Journey_to_the_West). Sun Wukong (the Monkey King), Zhu Bajie (Pigsy), and I assume the last one is Tang Sanzang (the Longevity Monk) are obviously not part of the Three Kingdoms history at all, but I didn't want to crop them out of the image either.

