## Yunchang’s Character Build

### AP

-   TODO
    -   Expected Accuracy with reasonable equipment
    -   DEX cap
    -   INT for HP washing and/or MP gain
    -   HP Challenges past level 100

### SP

#### Swordman

##### Skills List

|                   Skill | Max Level |                                                                                                     Description |                                               Max Level Effect |
|------------------------:|----------:|----------------------------------------------------------------------------------------------------------------:|---------------------------------------------------------------:|
|    Improved HP Recovery |        16 |                                                       Recover additional HP every 10 sec. while standing still. |                                      Recover additional HP +50 |
| Improved MaxHP Increase |        10 |                  This skill boosts up the amount of increase on MaxHP after each Level UP, or AP used on MaxHP. | If Level UP, +40 more; if AP applied, +30 more on top on MaxHP |
|                  Endure |         8 | Even when hanging on the rope or on a ladder, you’ll be able to recover some HP after a certain amount of time. |                                       Recover HP every 10 sec. |
|               Iron Body |        20 |                                                                      Temporarily increases your weapon defense. |                           MP -15; weapon def. +60 for 300 sec. |
|            Power Strike |        20 |                                                     Use MP to deliver a killer blow on a monster with a weapon. |                                            MP -12; damage 260% |
|             Slash Blast |        20 |                                               Use HP and MP to attack up to 6 monsters around you with a sword. |                                    HP -16, MP -14; damage 130% |

##### SP Build

| Level |  SP |                            Skill |                                                         Effect |
|------:|----:|---------------------------------:|---------------------------------------------------------------:|
|    10 |   1 |           1 Improved HP Recovery |                                       Recover additional HP +3 |
|    11 |   3 |           4 Improved HP Recovery |                                      Recover additional HP +12 |
|    12 |   1 |           5 Improved HP Recovery |                                      Recover additional HP +15 |
|    12 |   2 |        2 Improved MaxHP Increase |   If Level UP, +8 more; if AP applied, +6 more on top on MaxHP |
|    13 |   3 |        5 Improved MaxHP Increase | If Level UP, +20 more; if AP applied, +15 more on top on MaxHP |
|    14 |   3 |        8 Improved MaxHP Increase | If Level UP, +32 more; if AP applied, +24 more on top on MaxHP |
|    15 |   2 | (MAX) 10 Improved MaxHP Increase | If Level UP, +40 more; if AP applied, +30 more on top on MaxHP |
|    15 |   1 |                   1 Power Strike |                                             MP -4; damage 114% |
|    16 |   3 |                    3 Slash Blast |                                       HP -8, MP -6; damage 63% |
|    17 |   3 |                    6 Slash Blast |                                       HP -9, MP -7; damage 74% |
|    18 |   3 |                    9 Slash Blast |                                      HP -10, MP -8; damage 85% |
|    19 |   3 |                   12 Slash Blast |                                     HP -12, MP -10; damage 98% |
|    20 |   3 |                   15 Slash Blast |                                    HP -13, MP -11; damage 109% |
|    21 |   3 |                   18 Slash Blast |                                    HP -15, MP -13; damage 122% |
|    22 |   2 |            (MAX) 20 Slash Blast, |                                    HP -16, MP -14; damage 130% |
|    22 |   1 |                         1 Endure |                                       Recover HP every 31 sec. |
|    23 |   2 |                         3 Endure |                                       Recover HP every 25 sec. |
|    23 |   1 |                      1 Iron Body |                              MP -8; weapon def. +3 for 75 sec. |
|    24 |   3 |                      4 Iron Body |                            MP -8; weapon def. +12 for 105 sec. |
|    25 |   3 |                      7 Iron Body |                            MP -9; weapon def. +21 for 140 sec. |
|    26 |   3 |                     10 Iron Body |                           MP -10; weapon def. +30 for 175 sec. |
|    27 |   3 |                     13 Iron Body |                           MP -12; weapon def. +39 for 215 sec. |
|    28 |   3 |                     16 Iron Body |                           MP -13; weapon def. +48 for 250 sec. |
|    29 |   3 |                     19 Iron Body |                           MP -15; weapon def. +57 for 290 sec. |
|    30 |   1 |               (MAX) 20 Iron Body |                           MP -15; weapon def. +60 for 300 sec. |
|    30 |   2 |           7 Improved HP Recovery |                                      Recover additional HP +21 |

-   61 total SP
-   25 remaining SP after necessary 1st job skills
-   33 total unused SP COUNTING 2nd job SP

#### Spearman

##### Skills List

|                   Skill | Max Level |                                                                                                                                               Description |                                         Max Level Effect |
|------------------------:|----------:|----------------------------------------------------------------------------------------------------------------------------------------------------------:|---------------------------------------------------------:|
|           Spear Mastery |        20 |                                                                        Increases the spear mastery and accuracy. It only applies when a spear is in hand. |                         Spear mastery +60%, accuracy +20 |
|        Pole Arm Mastery |        20 |                                                              Increases the mastery of pole arms and accuracy. It only applies when a pole arm is in hand. |                      Pole Arm Mastery +60%, accuracy +20 |
|    Final Attack : Spear |        30 |                      Strikes an another, far deadlier blow following the initial attack with a given success rate. It works only when holding a pole arm. |    60% Success rate, final attack with spear damage 250% |
| Final Attack : Pole Arm |        30 | Strikes an another, far deadlier blow following the initial attack with a given success rate. It works only when holding a one-hand or two-hand pole arm. | 60% Success rate, final attack with pole arm damage 250% |
|           Spear Booster |        20 |                                Uses HP and MP to temporarily boost up the attacking speed of the equipped spear. It only applies when a spear is in hand. |     HP -10, MP -10; increase in spear speed for 200 sec. |
|        Pole Arm Booster |        20 |                          Uses HP and MP to temporarily boost up the attacking speed of the equipped pole arm. It only applies when a pole arm is in hand. |  HP -10, MP -10; increase in pole arm speed for 200 sec. |
|               Iron Will |        20 |                                                 Temporarily increases the level of weapon and magic defense on every member of the party around the area. |     MP -24; weapon def. +40, magic def. +40 for 300 sec. |
|              Hyper Body |        30 |                                                                  Temporarily increases the Max HP and Max MP of all members of the party around the area. |           MP -50; +60% on Max HP and Max MP for 155 sec. |

##### SP Build

| Level |  SP |                     Skill |                                                  Effect |
|------:|----:|--------------------------:|--------------------------------------------------------:|
|    30 |   1 |        1 Pole Arm Mastery |                      Pole Arm Mastery +15%, accuracy +1 |
|    31 |   3 |        4 Pole Arm Mastery |                      Pole Arm Mastery +20%, accuracy +4 |
|    32 |   3 |        7 Pole Arm Mastery |                      Pole Arm Mastery +30%, accuracy +7 |
|    33 |   3 |       10 Pole Arm Mastery |                     Pole Arm Mastery +35%, accuracy +10 |
|    34 |   3 |       13 Pole Arm Mastery |                     Pole Arm Mastery +45%, accuracy +13 |
|    35 |   3 |       16 Pole Arm Mastery |                     Pole Arm Mastery +50%, accuracy +16 |
|    36 |   3 |       19 Pole Arm Mastery |                     Pole Arm Mastery +60%, accuracy +19 |
|    37 |   3 |        3 Pole Arm Booster |  HP -27, MP -27; increase in pole arm speed for 30 sec. |
|    38 |   3 |        6 Pole Arm Booster |  HP -24, MP -24; increase in pole arm speed for 60 sec. |
|    39 |   3 |        9 Pole Arm Booster |  HP -21, MP -21; increase in pole arm speed for 90 sec. |
|    40 |   3 |       12 Pole Arm Booster | HP -18, MP -18; increase in pole arm speed for 120 sec. |
|    41 |   3 |       15 Pole Arm Booster | HP -15, MP -15; increase in pole arm speed for 150 sec. |
|    42 |   3 |       18 Pole Arm Booster | HP -12, MP -12; increase in pole arm speed for 180 sec. |
|    43 |   2 | (MAX) 20 Pole Arm Booster | HP -10, MP -10; increase in pole arm speed for 200 sec. |
|    43 |   1 | (MAX) 20 Pole Arm Mastery |                     Pole Arm Mastery +60%, accuracy +20 |
|    44 |   3 |               3 Iron Will |       MP -12; weapon def. +6, magic def. +6 for 45 sec. |
|    45 |   3 |              3 Hyper Body |            MP -25; +6% on Max HP and Max MP for 20 sec. |
|    46 |   3 |              6 Hyper Body |           MP -25; +12% on Max HP and Max MP for 35 sec. |
|    47 |   3 |              9 Hyper Body |           MP -25; +18% on Max HP and Max MP for 50 sec. |
|    48 |   3 |             12 Hyper Body |           MP -25; +24% on Max HP and Max MP for 65 sec. |
|    49 |   3 |             15 Hyper Body |           MP -25; +30% on Max HP and Max MP for 80 sec. |
|    50 |   3 |             18 Hyper Body |           MP -50; +36% on Max HP and Max MP for 95 sec. |
|    51 |   3 |             21 Hyper Body |          MP -50; +42% on Max HP and Max MP for 110 sec. |
|    52 |   3 |             24 Hyper Body |          MP -50; +48% on Max HP and Max MP for 125 sec. |
|    53 |   3 |             27 Hyper Body |          MP -50; +54% on Max HP and Max MP for 140 sec. |
|    54 |   3 |       (MAX) 30 Hyper Body |          MP -50; +60% on Max HP and Max MP for 155 sec. |
|    55 |   3 |           3 Spear Mastery |                         Spear mastery +20%, accuracy +3 |
|    56 |   3 |           6 Spear Mastery |                         Spear mastery +25%, accuracy +6 |
|    57 |   3 |           9 Spear Mastery |                         Spear mastery +35%, accuracy +9 |
|    58 |   3 |          12 Spear Mastery |                        Spear mastery +40%, accuracy +12 |
|    59 |   3 |          15 Spear Mastery |                        Spear mastery +50%, accuracy +15 |
|    60 |   3 |          18 Spear Mastery |                        Spear mastery +55%, accuracy +18 |
|    61 |   1 |          19 Spear Mastery |                        Spear mastery +60%, accuracy +19 |
|    61 |   2 |           2 Spear Booster |     HP -28, MP -28; increase in spear speed for 20 sec. |
|    62 |   3 |           5 Spear Booster |     HP -25, MP -25; increase in spear speed for 50 sec. |
|    63 |   3 |           8 Spear Booster |     HP -22, MP -22; increase in spear speed for 80 sec. |
|    64 |   3 |          11 Spear Booster |    HP -19, MP -19; increase in spear speed for 110 sec. |
|    65 |   3 |          14 Spear Booster |    HP -16, MP -16; increase in spear speed for 140 sec. |
|    66 |   3 |          17 Spear Booster |    HP -13, MP -13; increase in spear speed for 170 sec. |
|    67 |   3 |    (MAX) 20 Spear Booster |    HP -10, MP -10; increase in spear speed for 200 sec. |
|    68 |   1 |    (MAX) 20 Spear Mastery |                        Spear mastery +60%, accuracy +20 |
|    68 |   2 |    9 Improved HP Recovery |                               Recover additional HP +27 |
|    69 |   3 |   12 Improved HP Recovery |                               Recover additional HP +36 |
|    70 |   3 |   15 Improved HP Recovery |                               Recover additional HP +45 |

-   121 total SP
-   8 remaining SP after necessary 2nd job skills

#### Dragon Knight

##### Skills List

|                 Skill | Max Level |                                                                                                                               Description |                                                Max Level Effect |
|----------------------:|----------:|------------------------------------------------------------------------------------------------------------------------------------------:|----------------------------------------------------------------:|
|  Elemental Resistance |        20 |                                                              Gains resistance against all Magic attacks (Fire, Cold, Lightning & Poison.) |                  40% Increased resistance on every element type |
|         Spear Crusher |        30 |                                                                          Attacks multiple monsters serveral times by thrusting the spear. |                Use 24 MP, Damage 170%, Attack 3 Enemies 3 Times |
|      Pole Arm Crusher |        30 |                                                                       Attacks multiple monsters serveral times by thrusting the pole arm. |                Use 24 MP, Damage 170%, Attack 3 Enemies 3 Times |
|    Dragon Fury: Spear |        30 |                                                        Attacks up to 6 monsters nearby in relatively distant range by swinging the Spear. |                                   Use 30 HP, 20 MP, Damage 250% |
| Dragon Fury: Pole Arm |        30 |                                                     Attacks up to 6 monsters nearby in relatively distant range by swinging the pole arm. |                                   Use 30 HP, 20 MP, Damage 250% |
|             Sacrifice |        30 | Attacks a single monster while disabling the monster’s defending capacity. However, some damage will be done to the attacker him/herself. |            Use 18 MP, Damage 350%, Sacrifice HP by 5% of Damage |
|           Dragon Roar |        30 |                                                        Attacks up to 15 monsters at once. It works only when more than 50% of HP is left. |              Use 30 MP, 30% HP, Damage 240%, Attack Range 400%, |
|           Power Crash |        20 |                                                               Nullifies “power-up” skills of multiple monsters with a given success rate. |        Use 7 MP, Cancels out Power-Up Skill in a chance of 100% |
|          Dragon Blood |        20 |                             Increases the attacking capacity, but decreases HP steadily until 4 seconds before the remaining HP exhausts. | Use 24 MP, 20 HP in every 4 seconds, Attack + 12 in 160 Seconds |

##### SP Build

| Level |  SP |                          Skill |                                                Effect |
|------:|----:|-------------------------------:|------------------------------------------------------:|
|    70 |   1 |        1 Dragon Fury: Pole Arm |                          Use 20 HP, 10 MP, Damage 80% |
|    71 |   3 |        4 Dragon Fury: Pole Arm |                         Use 20 HP, 10 MP, Damage 110% |
|    72 |   3 |        7 Dragon Fury: Pole Arm |                         Use 20 HP, 10 MP, Damage 140% |
|    73 |   3 |       10 Dragon Fury: Pole Arm |                         Use 20 HP, 10 MP, Damage 170% |
|    74 |   3 |       13 Dragon Fury: Pole Arm |                         Use 25 HP, 15 MP, Damage 185% |
|    75 |   3 |       16 Dragon Fury: Pole Arm |                         Use 25 HP, 15 MP, Damage 200% |
|    76 |   3 |       19 Dragon Fury: Pole Arm |                         Use 25 HP, 15 MP, Damage 215% |
|    77 |   3 |       22 Dragon Fury: Pole Arm |                         Use 30 HP, 20 MP, Damage 226% |
|    78 |   3 |       25 Dragon Fury: Pole Arm |                         Use 30 HP, 20 MP, Damage 235% |
|    79 |   3 |       28 Dragon Fury: Pole Arm |                         Use 30 HP, 20 MP, Damage 244% |
|    80 |   2 | (MAX) 30 Dragon Fury: Pole Arm |                         Use 30 HP, 20 MP, Damage 250% |
|    80 |   1 |                    1 Sacrifice | Use 12 MP, Damage 205%, Sacrifice HP by 20% of Damage |
|    81 |   2 |                    3 Sacrifice | Use 12 MP, Damage 215%, Sacrifice HP by 19% of Damage |
|    81 |   1 |                  1 Dragon Roar |     Use 16 MP, 59% HP, Damage 96%, Attack Range 110%. |
|    82 |   3 |                  4 Dragon Roar |    Use 16 MP, 56% HP, Damage 114%, Attack Range 140%. |
|    83 |   3 |                  7 Dragon Roar |    Use 16 MP, 53% HP, Damage 132%, Attack Range 170%. |
|    84 |   3 |                 10 Dragon Roar |    Use 24 MP, 50% HP, Damage 150%, Attack Range 200%. |
|    85 |   3 |                 13 Dragon Roar |    Use 24 MP, 47% HP, Damage 165%, Attack Range 230%. |
|    86 |   3 |                 16 Dragon Roar |    Use 24 MP, 44% HP, Damage 180%, Attack Range 260%. |
|    87 |   3 |                 19 Dragon Roar |    Use 24 MP, 41% HP, Damage 195%, Attack Range 290%. |
|    88 |   3 |                 22 Dragon Roar |    Use 30 MP, 38% HP, Damage 208%, Attack Range 320%. |
|    89 |   3 |                 25 Dragon Roar |    Use 30 MP, 35% HP, Damage 220%, Attack Range 350%. |
|    90 |   3 |                 28 Dragon Roar |    Use 30 MP, 32% HP, Damage 232%, Attack Range 380%. |
|    91 |   2 |           (MAX) 30 Dragon Roar |    Use 30 MP, 30% HP, Damage 240%, Attack Range 400%. |
|    91 |   1 |         1 Elemental Resistance |        12% Increased resistance on every element type |
|    92 |   3 |         4 Elemental Resistance |        18% Increased resistance on every element type |
|    93 |   3 |         7 Elemental Resistance |        24% Increased resistance on every element type |
|    94 |   3 |        10 Elemental Resistance |        30% Increased resistance on every element type |
|    95 |   3 |        13 Elemental Resistance |        33% Increased resistance on every element type |
|    96 |   3 |        16 Elemental Resistance |        36% Increased resistance on every element type |
|    97 |   3 |        19 Elemental Resistance |        39% Increased resistance on every element type |
|    98 |   1 |  (MAX) 20 Elemental Resistance |        40% Increased resistance on every element type |
|    98 |   2 |                2 Spear Crusher |         Use 10 MP, Damage 60%, Attack 1 Enemy 2 Times |
|    99 |   3 |                5 Spear Crusher |         Use 10 MP, Damage 75%, Attack 1 Enemy 2 Times |
|   100 |   3 |                8 Spear Crusher |       Use 13 MP, Damage 90%, Attack 2 Enemies 2 Times |
|   101 |   3 |               11 Spear Crusher |      Use 16 MP, Damage 104%, Attack 3 Enemies 2 Times |
|   102 |   3 |               14 Spear Crusher |      Use 16 MP, Damage 116%, Attack 3 Enemies 2 Times |
|   103 |   3 |               17 Spear Crusher |        Use 19 MP, Damage 128%, Attack 1 Enemy 3 Times |
|   104 |   3 |               20 Spear Crusher |        Use 19 MP, Damage 140%, Attack 1 Enemy 3 Times |
|   105 |   3 |               23 Spear Crusher |      Use 21 MP, Damage 149%, Attack 2 Enemies 3 Times |
|   106 |   3 |               26 Spear Crusher |      Use 24 MP, Damage 158%, Attack 3 Enemies 3 Times |
|   107 |   3 |               29 Spear Crusher |      Use 24 MP, Damage 167%, Attack 3 Enemies 3 Times |
|   108 |   1 |         (MAX) 30 Spear Crusher |      Use 24 MP, Damage 170%, Attack 3 Enemies 3 Times |
|   108 |   2 |           2 Dragon Fury: Spear |                          Use 20 HP, 10 MP, Damage 90% |
|   109 |   3 |           5 Dragon Fury: Spear |                         Use 20 HP, 10 MP, Damage 120% |
|   110 |   3 |           8 Dragon Fury: Spear |                         Use 20 HP, 10 MP, Damage 150% |
|   111 |   3 |          11 Dragon Fury: Spear |                         Use 25 HP, 15 MP, Damage 175% |
|   112 |   3 |          14 Dragon Fury: Spear |                         Use 25 HP, 15 MP, Damage 190% |
|   113 |   3 |          17 Dragon Fury: Spear |                         Use 25 HP, 15 MP, Damage 205% |
|   114 |   3 |          20 Dragon Fury: Spear |                         Use 25 HP, 15 MP, Damage 220% |
|   115 |   3 |          23 Dragon Fury: Spear |                         Use 30 HP, 20 MP, Damage 229% |
|   116 |   3 |          26 Dragon Fury: Spear |                         Use 30 HP, 20 MP, Damage 238% |
|   117 |   3 |          29 Dragon Fury: Spear |                         Use 30 HP, 20 MP, Damage 247% |
|   118 |   1 |    (MAX) 30 Dragon Fury: Spear |                         Use 30 HP, 20 MP, Damage 250% |
|   118 |   2 |                         \#TODO |                                                \#TODO |
|   119 |   3 |                         \#TODO |                                                \#TODO |
|   120 |   3 |                         \#TODO |                                                \#TODO |

-   151 total SP
-   30 Dragon Fury: Pole Arm
-   3 Sacrifice
-   30 Dragon Roar
-   20 Elemental Resistance  
-   30 Spear Crusher
-   30 Dragon Fury: Spear
-   11 Sacrifice?
-   Dragon Blood instead of Sacrifice??
