![Chapter 5 Banner](./images/000_chapter_5_banner.png)

------------------------------------------------------------------------

## Chapter 5: Yunchang Versus the Giant Centipede

### Inturduction

Welcome back gamers and gametes. Did you like the custom image borders last chapter? I put a whole lot of effort into making them, even if some admittedly looked like shit (sorry about that, Thailand deserves better). Don’t worry, I’m going to refine them over time. I just really wanted that April 1st diary release date.

You probably recognize the font used for the image captions, too, because that’s literally the UI header font in Maple! I couldn’t find the actual font anywhere, and it seemed baked into the UI elements. So I just cut out the letters from some screenshots, made up the few missing characters and many symbols, then spent hours learning how to make it a usable font. Assuming the font doesn’t exist somewhere and I was just too dumb to find it, maybe I can release the font some time! It’s just a simple pixelly font, nothing too special.

Is this thing at all entertaining to read? I think the overly verbose fantasy angle is drawing out the action to a crawl, but this is really fun to write and make images for.

Anyway, let’s get farted.

------------------------------------------------------------------------

### Japan Desu

Back to the Mushroom Shrine! Youkoso and all that shit! Yunchang bows once more to the great torii, and sets her eyes on the local food stalls.

![Takoyaki, Ramen, and Delicious Dango!](./images/001_takoyaki_ramen_and_delicious_dango.png)

Yunchang uses her first Gachapon ticket! And gets her first trash scroll!

![First Gacha](./images/002_first_gacha.png)

Cape for LUK 60%… and Yunchang cannot even find any capes in the World Tour until level 50.

Yunchang feels a pull towards the strange well in the middle of the shrine. Just as she notices the weird yellow energy ball things emenating from the dark, she catches a strange sight. A wolf demon with a young girl?

![Inuyasha and Kagome at the Spirit Well](./images/003_inuyasha_and_kagome_at_the_spirit_well.png)

What? This must be one of those isekai anime spirit wells! Yunchang dives in head first…

She finds herself floating through an endless void. A force pulls her forwards, deeper into the dark. A light begins to shine around Yunchang as she is encapsulated in a sphere of pure spiritual energy.

![Yunchang Inside the Spirit Ball](./images/004_yunchang_inside_the_spirit_ball.png)

The sphere of energy begins to vibrate and crack until finally it ruptures into many pieces. Yunchang feels the unbearable pain of her body splitting apart.

![Yunchang Shatters](./images/005_yunchang_shatters.png)

In the great dark void, Yunchang can only think and dream. Time stretches on, forming an endless pool of lifeless experience. Days, months, years, lifetimes all pass before her. In her lonely mind, Yunchang feels the pain of death 1,000 times over as her old self is symbolically hurled through time and space.

![Kind of Like This](./images/006_kind_of_like_this.gif)

Yunchang finds herself transformed. With each barrage of pain, a bit more weakness leaves her body. The childish Yunchang of the past is no more. She is hardened into a true God of War – timeless, shapeless, infinite.

Finally, Yunchang finds peace. Stability. A fog begins to form. The fog becomes heavy, and drips into an boundless ocean. With her spear, Yunchang begins to churn the watery chaos until it hardens. Yunchang drops the thick brine from her speartip, and it forms into a great landmass.

![Yunchang Creates the Japanese Archipelago](./images/007_yunchang_creates_the_japanese_archipelago.gif)

Satisfied with her creation, Yunchang descends on to the island of Honshu.

------------------------------------------------------------------------

### A Blast Into the Past

Back on solid ground and in human form, Yunchang collects herself. She’s still in Japan, but it’s so… feudal! She remembers – using the magic of the well, she’s traveled into the past!

She wastes no time and buys a fresh pair of Silver War Boots from Teyandei… and immediately fails a Shoes for Jump 10% on them.

![New Boots, Same Old Scroll Failure](./images/008_new_boots_same_old_scroll_failure.png)

She enters the Ninja Castle. Judging by all of the arrows, throwing stars, and knives lodged into the walls, this castle must have been captured under siege! Yunchang switches to her snowboard, which is definitely a viable weapon, and gets to work on thinning out the Genin.

![Genin Slaying](./images/009_genin_slaying.png)

Rather than dropping dead, they simply turn into dolls. They don’t drop much of value, but these EXP rates have never been tastier.

Yunchang discovers her entrance to the castle. She explores the halls, slaying some Ashigaru and Genin, until she falls through a trap in the floor.

![Ishirasu Is a Cutie](./images/010_ishirasu_is_a_cutie.png)

She stumbles upon a grocer? In this warzone? Ishirasu claims his immense wealth comes from the many adventurers buying out his cider supply. Yunchang stocks up on some Hard Cider, gets tipsy, and gets back to battle. She hits level 32!

![Ding, Level 32!](./images/011_ding_level_32.png)

Excellent progress, Yunchang! The audience was wondering when you’d make literally any measurable progress outside of this ridiculous story!

She’s had enough of the Ninja Castle, and wants to get back to the future. Yunchang returns through the well, back to Mushroom Shrine, and gets to work on that photograph of the moon she had promised to the Baby Moon Bunny.

Luckily, the well is much kinder to Yunchang this time, and she does not have to suffer the pain of a thousand deaths! What a relief.

She heads deep into the woods. At the end of the Crow Forest lies a sign warning her not to go ahead.

![“Abunai!” – or “Danger!”](./images/012_abunai_or_danger.png)

Abunai indeed! Past the sign, the sun immediately drops out of the sky, and is replaced by a gigantic moon! And the place is swarming with horrific Zombie Mushrooms!

Yunchang tries to snap a photograph of the glorious moon over Fuji, but the camera’s shutter won’t budge.

![Maybe the Crescent Moon Isn’t Ideal for the Picture…](./images/013_maybe_the_crescent_moon_isnt_ideal_for_the_picture.png)

Yunchang traces her steps backwards, hoping to get a better angle. She stumbles upon many Paper Lantern Ghost yokai, but MISSes every stab. In this place, the full moon shines bright like a gentle lantern in the night sky. Yunchang tries to snap another picture!

![Maybe this Camera Should Go Fuck Itself…](./images/014_maybe_this_camera_should_go_fuck_itself.png)

FAILURE AGAIN. Sorry, Baby Moon Bunny, but your camera is borked to hell. She supposes that the Moon Bunnies would like a more Korean view of the moon. Too bad, the World Tour doesn’t offer any Korean destinations. Yunchang sighs and accepts that it wasn’t meant to be.

Still feeling a thirst for exploration, she heads past the Doubutsu no Mori (Forest of Animals) towards the Showa Town, a land basking eternally in a sunset bliss.

![Blissful Brick Bashing](./images/015_blissful_brick_bashing.png)

But the Yakuza patrolling the streets are too much for her, and are loaded up with modern firearms! Yunchang gets the hell out of there, quickly!

------------------------------------------------------------------------

### Iron Maiden

A short rest on the Mushroom Shrine bench later, Yunchang again partakes in the Bunny Space Program, rocketing off to the moon. In this meeting place between worlds, she switches her choice of guild.

![Ironman – Now Recruiting!](./images/016_ironman_now_recruiting.png)

She’s sad to part ways with Flow after becoming so familiar with the peepee stench of its members. Flow won’t be too far away – they are still present in the magical alliance chat! Ironman is new to the Suboptimal alliance and needs a bit of help to grow, and so Yunchang places her faith in her new guild.

Back to feudal Japan, and back to the Ninja Castle! Yunchang shreds the foot soldiers to bits until they surrender some equipment.

![A Fearsome Weapon for a Fearsome Warrior](./images/017_a_fearsome_weapon_for_a_fearsome_warrior.png)

A Janitor’s Mop! Some may laugh at this silly polearm, but Yunchang is pleased to wield any weapon she’s claimed from her enemies using her own two hands.

She also finds a White Kendo Robe, but I guess it’s for men only. BOO, de-gender the equips already!

The Ashigaru spit up blood as they are mopped to bits, and eventually, they also spit up a Blue Bamboo Hat! 4 STR by the way!

![Blue Bamboo Hat](./images/018_blue_bamboo_hat.png)

Yunchang has obtained everything she came to get. After hitting level 33…

![Like, Fucking Ding? Level 33? Let’s Fucking Go?](./images/019_like_fucking_ding_level_33_lets_fucking_go.png)

Yunchang reluctantly adds more base INT because this game is a piece of fucking trash and she’ll never be able to do a single HP challenge in her life! Then she takes her leave of the besieged castle. Back to Mushroom Shrine, already.

Taking after local shrine custom, Yunchang mops up some fresh offerings. Praise the mushroom spirits!

![Mopping Up Offerings](./images/020_mopping_up_offerings.png)

She approaches the Miko of the shrine, Kino Konoko, and receives a much less awkward response this time. Kino-chan needs help cleaning up the shrine, and asks Yunchang to swipe 100 Crow Feathers from around the forest. Luckily, our hero already has over 100 Crow Feathers. She promises to clean more feathers in the future – and she WILL, considering that Crows are one of few places where Shoes for Jump 30% can be obtained.

Kino-chan has another task for Yunchang: Eliminate 100 Fire Raccoons. Yunchang accepts, and wonders if they’ll be regular raccoons or [raccoon dogs](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) (also knows as tanuki). Off to the Doubutsu no Mori! Shimashou ne!

![Doubutsu no Mori](./images/021_doubutsu_no_mori.png)

It’s very peaceful here, but Yunchang must disturb the peace of nature. Sorry, tanukis and kitsunes, but you’re multiplying too fast and disturbing the shrinegoers. She slays many of their bastard offspring, and Kino-chan is quite pleased.

![Locally Made Fish Cakes](./images/022_locally_made_fish_cakes.png)

PEE MOP ACQUIRED. This thing is REALLY slow, probably due to all the pee it’s soaked up. Useless, but a great find.

![Pee Mop](./images/023_pee_mop.png)

Another fame for Yunchang! Yahoo!

![Kargo Fames Yunchang](./images/024_kargo_fames_yunchang.png)

And she gets some new earrings! One pair for when she hits level 35, and one pair to equip right now\~

![Neko’s Eye and Gold Earrings](./images/025_nekos_eye_and_gold_earrings.png)

Level 34, and more base INT acquired.

![Level 34 Status Update](./images/026_level_34_status_update.png)

More shrine offerings, and Yunchang mops them up gracefully.

![Mop, Mop](./images/027_mop_mop.png)

Back to the Doubutsu no Mori! Yunchang scores a pair of Mithril Battle Grieves! Anyone notice how this game can’t decide how to spell Greaves? Greave, Greaves, Grieves… just pick one!

![Mithril Battle Grieves](./images/028_mithril_battle_grieves.png)

Per tradition, they are below average stats, and Yunchang fails yet another Shoes for Jump 10%.

Level 35!!!!

![Oh Yeah!!!!](./images/029_oh_yeah.png)

Yunchang is feeling pretty powerful now. She tries again to push her way through the crowds of Yakuza…

![And There Were a LOT of Yakuza](./images/030_and_there_were_a_lot_of_yakuza.png)

Yunchang reaches Showa Town! First stop: the onsen, of course!

![A Warrior’s Rest](./images/031_a_warriors_rest.png)

Shinta’s weapon display hosts a number of rare weapons. Among them is the Bow of Magical Destruction. Yet another mystery, to be sure.

After buying some more Hard Cider, Yunchang stops by a woman wearing a beautiful pink kimono. Hanako-san asks for help gathering Cloud Foxtails to create a wonderful muffler. Yunchang is a muffler wearer herself, so hands over some of the MANY Foxtails she’s already gathered.

![Hanako’s Fuwafuwa Muffler](./images/032_hanakos_fuwafuwa_muffler.png)

Apparently, that isn’t good enough for Hanako-san. She insists that she asked for 300 Cloud Foxtails just a second ago, and scolds Yunchang. Yunchang wishes to declare bullshit, but just hands over the Foxtails. Enjoy, I guess.

![Experience Gaining Sounds](./images/033_experience_gaining_sounds.png)

Yunchang greets the Item Trader, and engages with the Showa Exchange for the first time! We’ll take a took at her reward compilation later on, during **The Bounty of the Han**.

She heads to leave the Showa Street Market, but is stopped by Hanako-san again. She demands that Yunchang gathers even more Cloud Foxtails, to make her a matching coat (to go with her already extravagant kimono and muffler). Whatever, as long as she’ll reward more of that phat experience.

Yunchang drinks a delicious Strawberry Milk, which doubles as a transportation potion, and is warped back to Kinoko Jinja!

She takes another trip to the moon, and gains another upgrade to her Octoblessed Ring (currently +4 all stats)! She “rescues” Gaga a few more times, getting an impressive enough time of 00:36.242, and then has a run-in with more Toilets. The Toilet asks if Yunchang would like to join the Toilet gang, but Yunchang has already dedicated herself to an iron-clad cause.

![TWEAKIN with the Toilets](./images/034_tweakin_with_the_toilets.png)

Yunchang meets with **`rusa`**, the least powerful Dark Knight to ever grace the land! What she lacks in strength, she makes up for in pure surfer attitude.

![Cowabunga, My Dudes](./images/035_cowabunga,_my_dudes.png)

The two swap fame, and go their separate ways.

Enough messing around; time to gather those Foxtails, Yunchang! She finds the lair of the Cloud Foxen (the official plural version of “Fox”), and beats many Foxen into a fine paste with her mop. Until…

![What’s This?](./images/036_whats_this.png)

They drop a Sapphire Fitted Mail! Oh my goodness, The Look™ is seriously coming together now! Yunchang is so blue, she almost looks like she could be serving under Cao Wei. You know, it all depends on your [color scheme of choice](https://www.quora.com/What-were-the-banner-colors-of-the-3-factions-in-the-Three-Kingdoms-period-of-China). I am so fucking good at finding reliable sources, no need to thank me for my intense research. If you like shitty [Koei](https://en.wikipedia.org/wiki/Koei) games like I do, then Cao Wei is blue, I guess.

After gathering the last of the Cloud Foxtails, Yunchang drinks some Fruit Milk, and instantly pops back to Showa Town. She tries her hand at another Showa Exchange, and then delivers the Foxtails to Hanako-san.

![A Great Bounty of Quest Experience](./images/037_a_great_bounty_of_quest_experience.png)

Yunchang hopes that Hanako-san is finally pleased, and won’t think up another bullshit task on the spot. By the way, did you know that Hanako-san is a total hippy? Her name literally means Flower Child. Knowledge is power.

Another teleportation beverage later, and Yunchang gets to chill on the Kinoko Jinja bench. And here comes the Beginner girl yet again. Yunchang’s got another spare Yellow Valentine Rose taking up space in her inventory, so lets the cute noob have it.

![A Rose for a Japanese Idol](./images/038_a_rose_for_a_japanese_idol.png)

After a break, Yunchang is ready to fire herself back into orbit. Specifically, onto the moon, which is orbiting Earth. This same moon orbits Maple World too, I guess?

Yunchang receives a task to slay 100 Sheep! Yes yes, another doable event raffle!

![Here We Go Again](./images/039_here_we_go_again.png)

But first, Yunchang does a bit of space mining on her Spacehorse, and some jump questing. She spots the eternal Beginner champion, **`OmokTeacher`**! He deserves a fame!

![God He’s So Stylish](./images/040_god_hes_so_stylish.png)

The Cloud Foxen hunt resumes! Yunchang plans to collect a few hundred more Cloud Foxtails, then hand them in at the Showa Exchange, hoping to get a Pink Bandana. If she’s lucky, she may even score a Newspaper Hat in the process.

But first, she gets an even better Sapphire Fitted Mail! 5 DEX, fucking epic!

![New Overall Alert](./images/041_new_overall_alert.png)

She chooses to walk to Showa Town this time, and mops away a Yakuza member’s life force using her, uh, Mop. 200 more Cloud Foxtails later, and the Exchange just gives her more trash potions. Yunchang doesn’t really like potions, and prefers to eat real food.

More Cloud Foxen hunting! Yunchang hits level 36!

![AWW YEAH!](./images/042_aww_yeah.png)

In the midst of the hunt, **Cortical** appears!

![…And Begins to Sekuhara Yunchang](./images/043_and_begins_to_sekuhara_yunchang.png)

After some [sekuhara](https://en.wiktionary.org/wiki/%E3%82%BB%E3%82%AF%E3%82%B7%E3%83%A3%E3%83%AB%E3%83%8F%E3%83%A9%E3%82%B9%E3%83%A1%E3%83%B3%E3%83%88#Japanese) between friends, the two share a bountiful feast of dango and dilk (short for rein**d**eer m**ilk**).

![A Dango and Dilk Feast!](./images/044_a_dango_and_dilk_feast.png)

And they engage in yet another grand battle of wits! Two studied military minds mash their brain folds together in an ultimate battle. The purity of Yunchang’s Ironwoman challenge hangs in the balance.

![The Stakes Are High](./images/045_the_stakes_are_high.png)

Of course, Yunchang loses. Her base INT must be a bit low. She accepts Cortical’s trade with tears welling in her eyes.

![A Sad Day for Ironmen](./images/046_a_sad_day_for_ironmen.png)

Cortical hangs around for a while, watching the Foxen get their brains bashed one by one. Yunchang takes Cortical’s continued taunts as a challenge to improve herself.

![Cortical Spouts More Demotivations](./images/047_cortical_spouts_more_demotivations.png)

Yunchang finds a Frying Pan, with a long-since-fried egg still attached! Given that this thing was also a part of Shinta’s weapon display, it must possess some rare ability that currently lies dormant. Or something.

With one last epic troll move, Cortical parts ways with Yunchang. She’s not sure if this scroll can warp one back to Shanghai, and she decides to not even try.

![Shanghai Wai-Tan Warp Scroll](./images/048_shanghai_wai_tan_warp_scroll.png)

Yunchang discovers the sword belonging to Diao Chan! I don’t recall Diao Chan owning a sword, so what is this? A prop sword for dancing? I doubt this thing is supposed to be the Seven Star Blade. Regardless, Yunchang keeps this magnificent relic of the Three Kingdoms!

![The Diao Chan Sword](./images/049_the_diao_chan_sword.png)

After a bit more slaying, Yunchang heads back to the shrine for a bit of good old fashioned hard labor.

![Volunteering at the Yakisoba Stand](./images/050_volunteering_at_the_yakisoba_stand.png)

And Yunchang runs into the friendly and extremely cute adventurer, **`AzureRain`**! After a small talk about Ironwoman restrictions, the two become friends, and Yunchang receives a fresh fame. Thanks, AzureRain!

![There’s Nothing Like a Whipping Between Friends](./images/051_theres_nothing_like_a_whipping_between_friends.png)

Oh yes! Yes, yes! Yunchang obtains a Newspaper Hat!

![The Newspaper Samurai Helmet](./images/052_the_newspaper_samurai_helmet.png)

And she hits level 37!

![The Mushroom Spirits Are Pleased](./images/053_the_mushroom_spirits_are_pleased.png)

Yunchang feels very satisfied with her progress. She owes a lot to the Kinoko Jinja, and decides to give back. She takes a few items from her pack, some of which were dear possessions, and offers them to the shrine. Just as life is fleeting, so is inventory space.

![Gotta Mop Those Offerings](./images/054_gotta_mop_those_offerings.png)

Yunchang returns to Showa Town on foot. The old man Tsuri-san eyes Yunchang, and catches her attention. Apparently, he has been searching for the Newspaper Hat. He seems to want to unfold it so that he can read “The Diary of the Snails,” a cartoon published daily in [The Maple Times](https://forum.maplelegends.com/index.php?threads/the-maple-times.49050/). A shoutout to **`Journalist`**, one of the masterminds behind this diligent news source!

![The Maple Times – Referenced In-Game!](./images/055_the_maple_times_referenced_in_game.png)

For returning the newspaper, now unfolded and no longer a Newspaper Hat, Yunchang receives an Overall Armor for DEX 30% scroll! In silence, she applies the scroll to her armor…

![Successful Scrolling!](./images/056_successful_scrolling.png)

Oh yeah. Just what she needed – her Fitted Mail now has 10 DEX, 3 Accuracy, and 1 Speed! Excellent!

Yunchang wants to capitalize on her luck, so hits up the Showa Exchange. She’s rewarded with TWO Pink Bandanas of equal stats – 8 Wdef, 3 Speed! She takes a trip back to the onsen, and uses her second Common Gachapon Ticket. Green Hunter’s Armor; trash, but sellable trash this time.

![Showa Spa Trashapon](./images/057_showa_spa_trashapon.png)

She drinks some milkies, teleporting back to Mushroom Shrine. She leaves a few more offerings, and mops them away, just the way Kamisama wishes.

![Mop, Mop, Mop!](./images/058_mop_mop_mop.png)

She checks the server time, and decides that she’s spent enough time in glorious Nippon. Raise the sails, or steam clouds, or whatever! Time to head back to Shanghai!

------------------------------------------------------------------------

### The Middle Kingdom

Back to Shanghai, a paradoxical place of towering cityscapes and flat farmlands! Yunchang notices that she can actually control the course of the river ferry by walking left or right.

![Controlling the River Ferry](./images/059_controlling_the_river_ferry.gif)

Time to get to work; 100 Sheep must bleat their last bleat. No screenshots needed, it was fucking easy. Yunchang offers up her Strawberry Milk of Teleportation to the great river Whangpoo, as she’ll need a Use slot.

![Offering to Whangpoo](./images/060_offering_to_whangpoo.png)

For her efforts, Yunchang is rewarded with Myo Myo the Traveling Salesman. I hope Myo Myo likes living in the Cash inventory, because that’s where it’s staying (this shop is forbidden in World Tour Lock). The good news is, Yunchang’s Octoblessed Ring is now +6 all stats!

Yunchang joins a party… with other players in it. Oh, this doesn’t look good with the Ironman guild tag. It’s just for a bit of fun at the jump quest, so you guys can keep a secret, right?

![It’s Party Time](./images/061_its_party_time.png)

The fastest time I was able to record was 00:33.696, but unfortunately that was not on Yunchang, so I can’t share the video here…

Yunchang gets two useless raffle tasks in a row. But her Octoblessed Ring becomes +7 all stats! Wowee!

Feeling confident and powerful, Yunchang is ready to defeat some Plow Oxen. Back to the fields! She equips her Pink Bandana for extra Speed, and gets to work.

![Yunchang Destroying Plow Oxen, Plus Level 37 Status Update](./images/062_yunchang_destroying_plow_oxen_plus_level_37_status_update.png)

Finally, she gathers 100 Plows! She receives the Tripod from Mr. Yang, the old farmer with a bad back.

![Tripod Get](./images/063_tripod_get.png)

Now, Yunchang can return this thing to Cho the photographer. With great disappointment in his demeanor, Cho takes her ID picture – he’s a lot less interested in Yunchang now that she’s clothed up.

![Cho the Pootographer](./images/064_cho_the_pootographer.png)

More ungulate slaying! Yunchang gets some sweet Skull Earrings, which she’ll save for when she hits level 50.

LEVEL 38, MOTHERDUCKERS!

![Quacking Some Cow Skulls](./images/065_quacking_some_cow_skulls.png)

Yunchang grabs another useless raffle that she cannot complete. Then, she gives up more dear possessions to the Whangpoo. Goodbye, earrings and axes\~

![For the River Whangpoo!](./images/066_for_the_river_whangpoo.png)

------------------------------------------------------------------------

### The Divine Physician’s Pharmacopeia

Heading back to the fields again (this is getting repetitive). She has a run-in with the Giant Centipede, and studies its attacks carefully…

![Studying the Giant Centipood](./images/067_studying_the_giant_centipood.png)

This centipede has a lot of attacks! It can gather electricity around its “ultimate legs,” or hind pincers, and shoot a single spine from the ground. It can create a weird barbed wire circle thing, and shoot a fuckton of spines from the ground! And lastly, it can unleash what looks like toxic fart gas all around itself. Yuck.

So what’s the fuckin’ deal with this centipede? Why is this thing in particular ravaging China’s farmlands? The answer is, I have no idea, but let’s unearth some bullshit anyway!

The Giant Centipede seems to resemble the [Chinese red-headed centipede](https://en.wikipedia.org/wiki/Chinese_red-headed_centipede), a centipede that is pretty darn huge in real life at 20 cm (8 in). It feeds on LIVE ANIMALS OR INSECTS by fucking blasting them with venom containing neurotoxin. My god…

I forgot basically everything I learned in my biology classes, but this is INDEED a [spooky toxin](https://en.wikipedia.org/wiki/Ssm_spooky_toxin). With this shit, they can fuckblast prey up to 15x its already ridiculous size, disrupting their cardiovascular, respiratory, muscular, and nervous systems all at once. No… no thanks.

![Evil Fucker](./images/068_evil_fucker.png)

Apparently this venom stuff has been concentrated into venom-based poisons (known as Gu, or also Ku I guess, I can’t read Chinese in any form…) for thousands of years in certain Chinese cultures. How did they concentrate the centipede juice into a powerful, potent poison? Well, from this [very good and always 100% correct source](https://en.wikipedia.org/wiki/Gu_%28poison%29) we can learn…

“The traditional preparation of gu poison involved sealing several venomous creatures (e.g., centipede, snake, scorpion) inside a closed container, where they devoured one another and allegedly concentrated their toxins into a single survivor, whose body would be fed upon by larvae until consumed. The last surviving larva held the complex poison. Gu was used in black magic practices such as manipulating sexual partners, creating malignant diseases, and causing death.”

Holy fuck. Apparently this shit can also be used as an antidote for gu poisoning itself! Let’s keep reading…

The 8th-century pharmacologist Chen Cangqi explains using venomous creatures both to produce and cure gu-poison.

“In general reptiles and insects, which are used to make ku, are cures for ku; therefore, if we know what ku is at work, we may remedy its effects. Against ku of snakes that of centipedes should be used, against ku of centipedes that of frogs, against ku of frogs that of snakes, and so on. Those varieties of ku, having the power of subduing each other, may also have a curative effect.”

Whatever you say, Chen Cangqi. Maybe you’re right, or maybe the [Divine Physician (and Ancient Cannabis Tincture Producer) Hua Tuo](https://en.wikipedia.org/w/index.php?title=Hua_Tuo) is rolling in his grave right now.

Despite containing one of the “[Five Deadly Venoms](https://en.wikipedia.org/wiki/Five_Poisons),” (which come from the following animals: snakes, scorpions, centipedes, toads, and spiders) these fuckers have some use in [traditional Chinese medicine](https://en.wikipedia.org/wiki/Traditional_Chinese_medicine), and can be eaten WHOLE (or prepared in various other ways) as either food or as a medicinal supplement. Why? Just why?

Apparently the “why” is that this neurotoxin shit contained in the whole, dried centipede “…are used whole to treat various medical issues, including joint problems (which is its major use), alopecia areata, stroke, convulsions, lymphangitis, lumps or masses, neoplasm, poisonous tumours, carbuncles, and snake bites.”

If I’m having a stroke, the last thing I want is a goddamn 8 inch long centipede crammed into my mouth, but sure. Joint problems though? Maplers, we have a solution to your poor-posture-induced back pain.

![Centipedes Eaten, Thigh Highs On. Yup, It’s Mapling Time.](./images/069_centipedes_eaten_thigh_highs_on_yup_its_mapling_time.png)

No more need for posture checks! Wow! Mr. Yang in Shanghai should try this shit.

------------------------------------------------------------------------

### Plowing the Fields

Back to the action! The moocows spit up a couple more interesting drops! Yunchang gets an Omok Table, which she’ll never be able to craft into a full Omok set – the World Tour only offers Orange Mushroom Omok Pieces. And she gets a Red Jangoon Armor top, with grey stats.

![Useless but Stylish Topwear](./images/070_useless_but_stylish_topwear.png)

She digs her mop into more cows. From one cow’s stomach(s) pops out a Jousting Helmet! Yunchang tries it on, but she can barely see, and she sweats up a storm inside the heated metal facetrap. Oh well, at least it can be sold for some Mesos.

![How Did FangBlade Wear This Thing?](./images/071_how_did_fangblade_wear_this_thing.png)

Yunchang finds a Red Whip that gives slightly more Speed, so offers her old whip to the Whangpoo.

![Whed Rip Gone](./images/072_whed_rip_gone.png)

She gathers some Rooster and Sheep cards, then gives up a bunch of Chicken Foots and Duck Eggs to Owner Yeo for more Corn Sticks and Fruit Sticks\~

In the cow fields again, Yunchang hits level 39!

![And She’s Fucking PUMPED](./images/073_and_shes_fucking_pumped.png)

Another useless, useless raffle. But let’s show off another status update.

![Level 39 Status Update](./images/074_level_39_status_update.png)

She’s holding on to that 1 extra AP until the Octoblessed Ring is fully blessed. Let’s take a look at Yunchang’s Monster Book, too!

![Monster Book Compilation](./images/075_monster_book_compilation.png)

------------------------------------------------------------------------

### Yunchang Versus the Giant Centipede

It’s time for revenge. Yunchang prepares to destroy the horrifying centipede. She prepares her mop and calms her mind. It’s VIDEO TIME.

![Yunchang Versus the Giant Centipede](./videos/yunchang_versus_the_giant_centipede.webm)

VICTORY! She even gets a Giant Centipede card! Oh fuck yes.

After all that, Officer Lim still won’t accept Yunchang’s help. Sometimes, you just have to let losers lose.

![Please Let Yunchang Help…](./images/076_please_let_yunchang_help.png)

Yunchang hunts some Black Goat cards to finish off the set. In the process, she hits the big 4-0!

![That Spells 40!](./images/077_that_spells_40.png)

Yunchang turns in 100 more Duck Egg for more Corn Stick. Yummer. She does more chores in space, and gets a fresh 100 Sheep raffle. While on the moon, she meets with the strongest Bishop in the world, **`cervid`**! The two swap fame!

![cervid and Yunchang Swap Fame](./images/078_cervid_and_yunchang_swap_fame.png)

She also spies some sweet cosplayers. As an anime fan, Yunchang appreciates the [Gabriel Dropout](https://myanimelist.net/anime/33731/Gabriel_DropOut) looks! She doesn’t get the chance to get their attention, and misses her screenshot of Raphiel, but oh well.

![Animetastic Cosplayers](./images/079_animetastic_cosplayers.png)

AND NOW, THE GREATEST RAFFLE REWARD EVER – nope, just food. Check out that FAT MESO STACK though!

![Dumblings](./images/080_dumblings.png)

rusa finds our hero relaxing in Shanghai Wai-Tan! They share a chat and some laughs.

![And Some Vomit](./images/081_and_some_vomit.png)

Another useless raffle… and another after that… and another after that… and another… ant anodher…

An uprising of angry rafflegoers breaks out.

![The Lion Tail Club](./images/082_the_lion_tail_club.png)

On the final day of the Lunar New Year event, Yunchang receives a 100 Genin slaying task.

Her reward? 3 Owls of Minerva…

![Bowel Movement](./images/083_bowel_movement.png)

As the Moon Bunny Space Program packs up to leave town, Yunchang spends her Rabbit Currency on some souvenirs, buying out the entire shop.

![Sayonara, Tsuki no Usagi\~](./images/084_sayonara_tsuki_no_usagi.png)

Yunchang puts on a brave face, but the tears start flowing as soon as she departs from the moon.

------------------------------------------------------------------------

### Chousen no Tsuki

Nope, it’s not time to say sayonara yet. Yunchang prepares for one last Moon Bunny quest.

![The Treetop Village, Yet Again](./images/085_the_treetop_village_yet_again.png)

Time for a travel montage!

![Flying High Above the Clouds](./images/086_flying_high_above_the_clouds.gif)

From the City of Orbs, she takes a crane ride, nestled in a sack dangling under the bird’s neck. This is perhaps the most dangerous part of her whole journey.

![Hold On for Dear Life](./images/087_hold_on_for_dear_life.gif)

She reaches the idyllic Mu Lung village, a place blossoming with peaches, aspiring panda monks, and martial arts masters (who are also pandas). This peaceful place tucked away in the mountains reminds Yunchang of the story of the [Peach Blossom Spring](https://en.wikipedia.org/wiki/The_Peach_Blossom_Spring).

![Yet Another Guandao Spotted](./images/088_yet_another_guandao_spotted.png)

But she’s got to keep moving. She asks the crane for another terrifying flight. As she nears the Herb Town, the clouds begin to thicken and grow heavy, blanketing the landscape.

She discovers the source of the great fog!

![The Largest Bong in the World](./images/089_the_largest_bong_in_the_world.gif)

After a few heavy puffs, Yunchang continues her quest. The dolphins assist her, taking her deep beneath the ocean, where she can somehow still breathe. Maple World is an odd place.

![Deep Sea Diving](./images/090_deep_sea_diving.png)

Still high as balls, she pops out of a well and into the Korean Folk Town! This is a mystical place where folk tales of old are brought to life.

![The Old Kay Eff Tea](./images/091_the_old_kay_eff_tea.gif)

Yunchang sets her eyes on the far off Black Mountain. She follows the forest trail…

![What In Tarnation?](./images/092_what_in_tarnation.png)

OUCH! A wild Moon Bunny strikes Yunchang with its pounder, while another bites her arm! What the fuck, Moon Bunnies? Apparently this competing faction of Moon Bunnies holds jealousy over not actually living on the moon, and wish to stall Yunchang’s quest.

She pushes forward, evading the bunny pounding.

And she comes face to face with the ghost of a scholar, who desperately appeals to Yunchang for release.

![Scholarly Spirits](./images/093_scholarly_spirits.png)

But this Korean town and countryside is merely a figment of Yunchang’s blazed as fuck imagination, so she does not interfere.

She climbs to the highest peak of Black Mountain.

![Atop Black Mountain](./images/094_atop_black_mountain.png)

Yunchang brandishes the Instant Camera for one last photo attempt. She aims her lens at the moon, and hears a shutter sound. SUCCESS!

She traces her steps back, and can’t wait to return to the Baby Moon Bunny.

Along the way, she spots a huge shell that wasn’t present before.

![The Wondrous Seruf Shell](./images/095_the_wondrous_seruf_shell.png)

And takes a few more giant puffs of Herb Town reefer before taking a dip.

![Herb Town Spa](./images/096_herb_town_spa.png)

Alright, that’s enough fun, and she’s ready to depart for real. Back in the Treetop Village, Yunchang sets off once more for the archipelago of Zipangu, so she can finally rocket away to the moon.

Baby Moon Bunny is so pleased with the photograph, and Yunchang feels overwhelmed with immense pride. She receives perhaps the greatest souvenir of all, with which she can always remember her bunny friends.

![Pink Hare Chair](./images/097_pink_hare_chair.png)

Yunchang departs for the last time from the moon, waving a final goodbye with a smile.

------------------------------------------------------------------------

### The Bounty of the Han

There’s a lot to cover this time! Many monster cards from two different regions, Showa Exchanges, and Gachapon rewards! Let’s get to it.

![The Great Bounty of the Han feat. Level1Crook](./images/098_the_great_bounty_of_the_han_feat_level1crook.png)

Okay, I definitely fucked up and forgot to screenshot some of these monster cards, both in this entry and the last one. Let’s, uh, do a full card tally here to wrap up these numbers for sure.

**Monster Book**

-   Red
    -   Orange Mushroom (5/5)
    -   Green Mushroom (5/5)
-   Orange
    -   Blue Mushroom (5/5)
    -   Crow (1/5)
    -   Fire Raccoon (5/5)
    -   Cloud Fox (5/5)
    -   Genin (1/5)
    -   Rooster (5/5)
    -   Duck (5/5)
    -   Sheep (5/5)
    -   Goat (5/5)
-   Lime
    -   Ligator (2/5)
    -   Black Goat (4/5)
    -   Cow (5/5)
    -   Plow Ox (5/5)
-   Yellow
    -   Giant Centipede (1/5)

Woah there! Let’s not forget about all the NX and Gacha Tickets Yunchang got! There’s so fucking many, yeah I’m kinda lucky, fuckin’ deal with it nerd.

![The Bounty of the Han: IN SPACE](./images/099_the_bounty_of_the_han_in_space.png)

Pretty sweet right?

**Showa Exchange Results**

Total Equips Obtained: 2

-   Pink Bandana
    -   8 Wdef
    -   3 Speed
-   Pink Bandana
    -   8 Wdef
    -   3 Speed

*100 Raccoon Firewood*

Total attempts: 4

-   5 White Potion
-   15 Takoyaki (Octopus Ball)
-   20 Blue Potion
-   Mana Elixir

*100 Crow Feather*

Total attempts: 1

-   Mana Elixir

*100 Cloud Foxtail*

Total attempts: 13

-   10 Blue Potion
-   20 Blue Potion x4
-   10 White Potion
-   20 White Potion x2
-   Pink Bandana x2
-   Mana Elixir x2
-   15 Takoyaki (Octopus Ball)

Yep, lots of trash! Those Pink Bandanas are pretty sexy though.

**Common Gachapon**

Total Tickets Used: 2

-   Scroll for Cape for LUK 60%
-   Green Hunter’s Armor

**Rare Gachapon**

Total Tickets Used: 0

Trashapon…

**The Great Bounty:**

-   3,058,213 Mesos
-   12,250 Vote Cash
-   18 Common Gacha
-   6 Rare Gacha

A little clarification, **The Great Bounty** totals up every gachapon ticket Yunchang has ever gotten, including USED Gacha Tickets. The Meso and Vote Cash counts represent only what Yunchang currently has at the conclusion of the chapter. She has not spent any Vote Cash yet! Nope, not even on vomit faces! All vomit scenes thus far were created using state of the art CGI technology.

What’s the difference between a GREAT Bounty of the Han and a regular Bounty of the Han? Nothing at all really. It’s just nice to mix it up.

------------------------------------------------------------------------

### Yunchang’s List of Challenges, Restrictions, and Exceptions

In case you have no fucking idea what’s happening with Yunchang in this playthrough, but have been reading along anyway…

**Ongoing Challenges**

-   [**The World Tour Challenge**](https://codeberg.org/Taima/world_tour)
    -   Status: **Ongoing Foreverial for All Time**
    -   Summary: Yunchang must spend her entire life inside Spinel’s World Tour, unless an exception allows leaving. Event maps connected to the World Tour are always allowed.
-   **The Six Weapons of the Three Kingdoms**
    -   Status: **Ongoing, 1/6 Collected**
    -   Summary: Within the World Tour are six weapons belonging to legendary Chinese historical figures. Obtaining all six of these might do something, maybe? This seems like an okay goal to have.
        -   **Diao Chan Sword** obtained on March 16, 2023 from a Cloud Fox at Zipangu : The Mountain of Cloud Fox.
-   **No Fap Challenge**
    -   Status: **Ongoing, and the Urges Will Never Cease**
    -   Summary: Just don’t masturbate, ever. What would the twenty four emperors of the Great Han think if they saw you spurting dick milk? Have some self respect.

**Completed Challenges**

-   **Naked Challenge**
    -   Status: **Complete**
    -   Summary: Upon entering the World Tour, Yunchang is naked. Shops will refuse to sell items or provide services to Yunchang unless she is fully clothed. Interactions with other NPCs will also be more awkward. When completely naked, it’s better to just live in the wilderness. Yunchang completed this challenge on March 13, 2023 by obtaining a Yellow Engrit from a Ligator at Thailand : Toad Pond.

**Restrictions**

-   **Ironwoman:** You all know what Ironman is already. No trade, no party, no fun ever.
-   **Slow Travel:** Upon entering one of four self-contained World Tour zones (Shanghai, Shaolin Temple, Thailand, Zipangu), Yunchang may not travel via Spinel for three real-time days. Every server reset subtracts one full day.
-   **No Vote:** Vote Cash may only be obtained through monster drops and event rewards.
-   **No FM/CS/Channel Hop Abuse:** No warping around using these methods for faster travel. Use of the Free Market button at all is forbidden. AFKing in the Cash Shop is always permitted.

[**Exceptions**](https://codeberg.org/Taima/world_tour#exceptions)

These are tougher to summarize, and there’s a lot of small exceptions I don’t want to list individually here. Let’s just list off any new exceptions added. Here’s some fresh event-specific exceptions.

-   Partying/trading with other characters for harmless fun that does not result in gaining items, Mesos, experience, or forward in-game progress in any other way is okay (but still a little sketchy). This includes partaking in the Lunar New Year 2023 Jump Quest with friends, or being forced to accept an empty trade with Cortical after losing a battle of minds.
-   The **Slow Travel** restriction may be lifted to access daily event raffles. The raffle must take place within the World Tour. The **Slow Travel** three day countdown begins anew after entering the zone one wishes to complete a raffle in.
-   Leaving the World Tour is permitted to complete the following quest(s):
    -   “What does the moon look like?” from Baby Moon Bunny (Lunar New Year 2023)

Baby Moon Bunny’s quest has been added as an exception to the **World Tour Challenge** rules, if those weren’t enough of an unreadable mess already.

Let it be known that Yunchang followed her Slow Travel restriction completely during this event! The viable raffles were perfectly spaced out, or took place in the same area on consecutive days. I’m just adding this exception for the future.

Oh and if Event AP Resets don’t come back next event I’m probably junking that **No Vote** restriction. I don’t think anyone cares about that one much.

------------------------------------------------------------------------

### Interview with the Author, Part III

**Q:** Reading your diary gave me terminal butt rabies. Now I need to spend the rest of my short life hooked up to a butt ventilator. What do you have to say for yourself, you sick freak?

**A:** This is the intended effect of reading **Guan Yunchang’s World Tour Diary**! I feel blessed to have such dedicated fans.

**Q:** Hey, in last entry’s **Bromance of the Three Kingdoms, Part II** section you seemed to categorize characters from [Journey to the West](https://en.wikipedia.org/wiki/Journey_to_the_West) as Romance of the Three Kingdoms characters! Don’t you know a fucking thing about Chinese literature, you ignorant Western piece of shit?

**A:** I definitely forgot to mention these guys specifically, and I’ll add in a disclaimer to the [master Bromance post](https://codeberg.org/Taima/world_tour_diary/src/branch/main/bromance_of_the_three_kingdoms.md) about them. Sun Wukong (the Monkey King), Zhu Bajie (Pigsy), and I assume the last one is Tang Sanzang (the Longevity Monk) are obviously not part of the Three Kingdoms history at all, but I didn’t want to crop them out of the image either. Oh also feel free to correct me if I got their names wrong. I don’t actually know that much about Journey to the West, I’ve only ever seen a film rendition and it was a comedy…

![Yunchang Gets Hacked!](./images/100_yunchang_gets_hacked.png)

Guys, Yunchang got hacked and lost all her shit on April 1st, 2023. I’m so UPSET just looking at this login screen I won’t ever log in ever again. If anyone knows the culprit please report them to the GMs so they can be sent to a prison colony in Siberia. Thank you.

Bai, nee\~
