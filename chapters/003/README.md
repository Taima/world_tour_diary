![Chapter 4 Banner](./images/000_chapter_4_banner.png)

------------------------------------------------------------------------

## Chapter 4: The Water Margin

### The Big Poop

Welcome back to the land of poops and pees. I think we’ve finally shaken off the fleas of the MapleLegends forums, now all that remains are strong, healthy leeches! Dig into my skin and suck out the vital essence, my little leeches! Mwahahahaha!

By the way, this event is pretty kewl. I rate it McChicken out of Big Mac.

![Rabbit Currency](./images/001_rabbit_currency.png)

Let’s catch up with Yunchang in Japan!

------------------------------------------------------------------------

### To the Moon and Back

Last time, Yunchang left off very close to level 30, so let’s pop that levelup right now.

![LEVEL 30](./images/002_level_30.png)

On her way towards Spinel, she comes across a strange fortune teller. Or so she appears. She doesn’t tell any fortunes or have any interesting riddles, but just wants to warp Yunchang somewhere apparently.

*Warp noises*

Yunchang discovers a Bunny Space Program! These bunnies have staged a lunar mission, and are packing some serious technology.

![Moon Bunny Space Program](./images/003_moon_bunny_space_program.gif)

The bunnies urge Yunchang to jump! Against her better judgement, she does! She’s rocketed straight to the lunar surface!

She speaks with the bunnies of the moon colony, and gets a Cow raffle task. Nice score! She’ll have to go to Shanghai later to do that raffle.

![Cow Raffle](./images/004_cow_raffle.png)

Yunchang is ready for more training. She sees no need to keep working on her Warrior Basics – she’s ready for some Spearman Techniques. Off to Loseria Island she goes!

![Lightbulb Screaming Sounds](./images/005_lightbulb_screaming_sounds.png)

She takes a solemn walk from the bricked cityscape, up the crags, to the Warrior Stronghold. She takes a breath before entering Dances with Balrog’s ~~pleasure chamber~~ personal chamber. He greets Yunchang with a hearty hello, but she’s really not in the mood for more bullshit. She takes note that she’ll have to prove her worth as a warrior to advance in her training, and snatches away Dancing with Balrog’s Letter.

![Taking on the 2nd Job Advancement](./images/006_taking_on_the_2nd_job_advancement.png)

She climbs the West Rocky Mountain, her bare skin blistering as she goes. Finally, Yunchang reaches the Warrior Job Instructor. This guy won’t give us his name, that’s how fucking epic and mysterious he is.

![The Nameless Instructor](./images/007_the_nameless_instructor.png)

He warns Yunchang of the grave danger she’s about to face. Alone, she climbs to the mountain peak – the Warrior’s Rocky Mountain.

Lupins toss bananas and let loose their piercing monkey screech. Fire Boars charge with their scorching manes trailing behind them, their tusks stabbing deep into Yunchang’s flesh.

She beats them back, and picks up a single Dark Marble, but the horde overcomes her wild slashes. She drops to the ground, and a flaming boar stomps its hooves, cracking her skull and splattering Yunchang’s base INT right down the cliffside. A glorious death for a failed warrior.

![Yunchang Fucking DIES and Fails Her Test](./images/008_yunchang_fucking_dies_and_fails_her_test.png)

------------------------------------------------------------------------

### Chu Shi Biao, or a Memorial on the Case to Go to War

In the rift between worlds, Guan Yu awakens in the darkness. Before her eyes, a great portal splits open, and a man steps out, brandishing a great feathered fan.

“Guan Yu. You are a disappointment to your sworn brothers! Your ancestors weep at the sight of your misfortune. Please, try harder.”

The grand strategist of Shu Han looks down in shame at our hero.

“Yes, Kongming. I have died time after time, and with each bitter end I am thrown back into limbo between Maple World and the real world. Under such strict restrictions, how can I stand this great punishment?”

Speaking softly, Sleeping Dragon retorts, “I was of humble origin, and used to lead the life of a peasant in Nanyang. In those days, I only hoped to survive in such a chaotic era. I did not aspire to become famous among nobles and aristocrats.”

Sleeping Dragon takes a breath. Oh, it’s going to be a long, flowery speech yet again.

![Sleeping Dragon](./images/009_sleeping_dragon.png)

“The Late Emperor did not look down on me because of my background. He humbled himself and visited me thrice in the thatched cottage, where he consulted me on the affairs of our time. I was so deeply touched that I promised to do my best for the Late Emperor. We encountered hard times and setbacks later. I was given heavy responsibilities when we were facing defeats. I received important duties in dangerous and difficult situations. It has been ***\[STATIC\]*** years since then.”

“Well, I suppose that helps. Wait, the Late Emperor? What’s happened? Kongming!”

“…with deference and prudence, to the state of one’s depletion; it’s never finished until one’s death.”

Yunchang is drawn out of her reverie with the intangible spirit of Zhuge Liang, and crashes back down into the Cliffside Village, her body aching.

------------------------------------------------------------------------

### The Sleeping Dragon Knight

With renewed courage, Yunchang knows what to do. She gets the fuck up, and climbs to the Warrior Job Instructor yet again. She unleashes her dormant, repressed fury upon the elite piggos and monkes. To sustain her power, she chomps down 20 Bento boxes. Finally, victory at last! Yunchang is a true hero.

![Official Recognition of Hero Status](./images/010_official_recognition_of_hero_status.png)

And she’s also a Spearman! Dances with Balrog shares a few surprisingly wise words about using one’s power for the right purpose.

![Spearwoman](./images/011_spearwoman.png)

Time to explore a bit before heading home.

![Catte](./images/012_catte.png)

She puts her Useless Dumb Shitter Mushroom-themed cards into a deck!

![Match Cardz](./images/013_match_cardz.png)

And poisons the water supply.

![Henesys Cholera Outbreak of 2023](./images/014_henesys_cholera_outbreak_of_2023.png)

Maybe that was a bad idea. Time to get out of town.

![Henesys Residents Are Furious](./images/015_henesys_residents_are_furious.gif)

She heads back to Spinel, and takes the boat to Shanghai before the residents catch on.

------------------------------------------------------------------------

### The Bund

Along the Huangpu river bend stands a city divided in two. On one side, Pudong’s skyscrapers and financial buildings create a unique and wondrous skyline. On the other, Wai-Tan houses many historic buildings that were once foreign concessions. Don’t these old colonial relics seem dwarfed by the towering developments since then?

Yunchang steps off the boat, eager and ready to begin the long process of honing her Spearman techniques. Her first task – hunt the fearsome Cows for the Lost Grey.

![Dragons Detected](./images/016_dragons_detected.png)

After taking in the sights, she leaves the bustling city in search of the farmlands. But what’s this? The animals that once passively allowed themselves to be chopped into fresh McNuggets have staged a revolt! Ducks, Roosters, Sheep, Cows – no animal is safe anymore.

Progressing onwards and slapping away farm animals as she goes, Yunchang has a run-in with the horrifying Giant Centipede. She evades its stabbing pincers, but to her surprise, a series of spines shoot from the ground, eviscerating her entire body!

![Death by Giant Centipede](./images/017_death_by_giant_centipede.png)

She begins to plot her revenge, but clearly needs more training first.

Yunchang finds a new home: Split Road at West. What is it? A split road. Where is it? At west. This place has many safe spots from which she can slice up the animals!

Sky Blue Starry Bandana spotted, and it’s right next to a Shep card! Yoink! And not long after that, a second Bandaner drops.

![Sky Blue Starry Bandaners](./images/018_sky_blue_starry_bandaners.png)

SURRA visits Shanghai! Woah! SURRAAAAAAAAAAAAAA!

![Surra.](./images/019_surra.png)

These cows are tough. Yunchang sustains many a cow bite, avoids their charges, and stabs them over a dozen times each until they crumple into raw beef.

![100 Cows Beefed](./images/020_100_cows_beefed.png)

100 Cows fucking dunked. Yunchang trampolines directly to the moon, and returns to the Lost Grey. While Yunchang didn’t come any closer to finding the alien’s friends, she still receives a New Year’s Lion Body as her reward.

![New Year’s Lion Body](./images/021_new_years_lion_body.png)

Yunchang politely equips the Lion Body, and utters her muffled thanks from beneath the festive costume piece.

The moon bunnies are no stranger to nudity, so react kindly to Lady Yunchang. She continues to help the bunnies.

![Yunchang Harvests the Rare Moon Flowers](./images/022_yunchang_harvests_the_rare_moon_flowers.png)

Baby Moon Bunny apparently can’t get enough of the moon despite living on it. Yunchang is tasked with taking a picture of the moon from far away using an Instant Camera. She knows just the place to take a beautiful picture of the moon, but it’s going to be a while before she gets back there!

![Instant Camera](./images/023_instant_camera.png)

Yunchang meets Kimberly, the server administrator! Wowee! Kimberly is clearly too busy to respond, but Yunchang fames her. Kimberly can repay the favor any time, no rush.

![Faming the Real Kimmy](./images/024_faming_the_real_kimmy.png)

The Lost Grey asks for help again – and wants Yunchang to defeat 100 Cows, AGAIN. Do all aliens have a weird cow obsession?

![With a Slash of Her Spear, Yunchang Descends from Space](./images/025_with_a_slash_of_her_spear_yunchang_descends_from_space.png)

And Yunchang fucking DIES AGAIN TO THE GODDAMN CENTIPEDE.

![Oops](./images/026_oops.png)

Hang on, let’s see that again in glorious high definition.

![The Giant Centipede Devours Yunchang Alive](./images/027_the_giant_centipede_devours_yunchang_alive.png)

Yunchang heads back to the city for some rest and attempts to plop down on the least comfortable looking bench maybe fucking ever. And it’s fake! Unsittable! The empire has fallen into disrepair! Guan Yu is again emboldened on her great mission.

![Fake Bench](./images/028_fake_bench.png)

A Bow of Magical Destruction? What? Yunchang needs to keep this bow from falling into the wrong hands.

![BMD feat. Eggs](./images/029_bmd_feat_eggs.png)

Fucking dead AGAIN THANKS TO THE EVIL CHICKENS.

![Pecked to Death](./images/030_pecked_to_death.png)

Back at the Wai-Tan, Yunchang is accosted by Cho the photographer snapping pictures of her freshly resurrected body. Yes, this is actual MapleStory dialogue.

![Cho the Creepshotter](./images/031_cho_the_creepshotter.png)

Before he can take ecchi photographs of our hero, Cho needs his camera Tripod back! Yunchang is tasked with reclaiming it from Mr. Yang out in the fields.

Another man calls Yunchang over, and Yunchang sighs before stomping over. She’s surprised to find his demeanor slightly less creepy than Cho’s. The guy just needs some Chicken Feet and Duck Eggs for his recipes. Simple enough for Yunchang! She’s been busy slaying the farm beasts for hours now.

Yunchang came prepared. She hands Owner Yeo 100 Duck Eggs and is rewarded with 10 Corn Sticks. Yum!

![Achievement Unlocked: Corn](./images/032_achievement_unlocked_corn.png)

Yunchang has had enough of the city, and decides to get back to work in the fields. The city fades away, and is replaced by gentle winds and the sounds of angry farm animals.

Alongside the main path are many strange pillars. Yunchang notices footholds, perfect for climbing. She grapples her way up the strange structure…

![Climbing the Mysterious Pillar](./images/033_climbing_the_mysterious_pillar.png)

At the top, she hears a gentle hum from the three Threads of Fate strung across the pillars. Yunchang decides not to disturb the balance of fate and future, but takes note of the importance of these spiritual monuments.

Back at the Split Road (@ West)! Yunchang approaches Mr. Yang, an old man plowing the fields by hand, to his spinal detriment. His plow oxen have gone fucking nuts with rage, are tilling the fields in mad zigzag patterns, shaking off their plow equipment. To receive the camera Tripod for ecchi photo man back in the city, Yunchang must gather 100 of their plows.

![Yunchang Meets Mr. Feet](./images/034_yunchang_meets_mr_feet.png)

…But the plow oxen are way above Yunchang’s combat proficiency. They are not only stronger, but also smarter than Yunchang. An ambush party of plow oxen rides down the hills, their plows cracking and breaking behind them. Yunchang retreats back to the Split Road, and decides to focus on her simple Cow extermination task.

First, she attempts to scroll her Mithril War Boots with a Shoes for Jump 10%. It fails, of course.

It’s not easy work, but it builds character, or furthers her character build, or something. Many a cow is spitroasted on her spear, many a sheep bleats for mercy as she slays the rebellious animals.

For they will grant her no mercy, either.

![The Steaks Are High](./images/035_the_steaks_are_high.png)

Yunchang finds a Red Whip! A whip is no weapon for a spearwoman, but these whips grant great powers of Speed!

![Red Whip of Fastness](./images/036_red_whip_of_fastness.png)

Did we mention Yunchang has found some cards? Later on we’ll do an epic card gathering montage.

Yunchang has had enough of turning cows to beef, and takes a break. Back to the town she goes. And up to the moon she is thrown by the odd moon bunnies.

This moon is a meeting place between worlds! Here, Maplers and humans alike can meet. Kimmy is here again on her alt account, but has chronic AFKitis.

![Watching Us Like the Eye of Sauron](./images/037_watching_us_like_the_eye_of_sauron.png)

The part-time bunnies have also doubled their workforce!

![yay](./images/038_yay.png)

A bit of pre- and post-server reset Moon Flower gathering later…

![Octoblessed Ring obtained](./images/039_octoblessed_ring_obtained.png)

Yunchang obtains the Octoblessed Ring, as well as a Charm with which to bless it!

![Blessed](./images/040_blessed.png)

The rabbit-alien alliance provides free Spacehorses to any who want them, apparently. Yunchang partakes in some rabbit fun…

![Zap](./images/041_zap.png)

While Gaga partakes in some gaylien fun…

![Abducted by Gayliens](./images/042_abducted_by_gayliens.png)

Yunchang also helps out at the Space Mine, gathering whatever these crystals even are. She learns to perform the Space Dash in her Spacehorse!

The Maplers are enjoying the Spacehorses, too. Many partake in the Rabbit Games.

![Spacehorsemen of the Cockpocalypse](./images/043_spacehorsemen_of_the_cockpocalypse.png)

Feeling eight times blessed by her new ring, Yunchang returns to Earth, ready to fight the Beef War again.

Stab, stab, slash, and stab. Yunchang hits level 31, and another 100 Cows get beefified! These events did not happen at the same time.

![100 Cows Bonked](./images/044_100_cows_bonked.png)

Before returning to town, Yunchang finishes gathering 100 Chicken Feet for Owner Yeo.

Chicken FEETS returned! Yunchang tucks her 5 Fruit Sticks into her pocket for later, immediately ruining them with pocket lint.

![Chicken FEETS](./images/045_chicken_feets.png)

The New Year’s celebration has really taken off here in Shanghai. A lone lion head drifts by. Yunchang knows just what to do! Yunchang joins the Lion Dance\~

![Attempted New Year’s Lion](./images/046_attempted_new_years_lion.png)

The rabbits propel Yunchang back to the moon. She returns to the Lost Grey, and receives quite the prize!

![The Warmest Muffler](./images/047_the_warmest_muffler.png)

Pendants are hard to come across in World Tour Lock, so this is huge! An alliance member was also hunting for a muffler at the same time, but it seems the will of heaven was with Yunchang instead.

Yunchang slides down a moon hole, and explores the other side of the moon. There’s a giant Spacehorse here. Yunchang tries to peer inside the craft, but it’s too dark inside. The purple gayliens are quite a mystery.

![The Dark Side of the Moon](./images/048_the_dark_side_of_the_moon.png)

Yunchang plummets back down to Earth, somehow unscathed. She remembers that she has another 100 Duck Eggs for Owner Yeo, and gets another 10 Corn Sticks. Yum yum!

Yunchang spots the Guandao again! She tries to wave down the wielder, but it’s no use. Where are these things coming from?

![Guandao User Spotted](./images/049_guandao_user_spotted.png)

Oh shit, Yunchang spots an error in the Monster Book. Goats do NOT drop a [Blue Engrit](https://maplelegends.com/lib/equip?id=1051012)! She’s going to be more careful and look up what the monsters drop from now on, rather than relying on this flimsy book of fucking lies.

![Book of Lies](./images/050_book_of_lies.png)

For now, Yunchang feels she’s run through her last scrap of luck here in Shanghai. She turns her attention to the waterfront, and the flowing Huangpu River. Yunchang decides she actually prefers the old romanization, Whangpoo, instead.

Like a water droplet in the gushing river, Yunchang cannot remain stagnant. She’s got to fix her past mistakes and overcome her shame. She knows just what to do.

![The Adventure Continues](./images/051_the_adventure_continues.png)

------------------------------------------------------------------------

### The Water Margin

A perilous journey across land and sea.

A young woman who has yet to know her potential.

This potential is a power that could either…

Destroy her or realize her will.

Her courage shall determine her fate.

![Yunchang Steps Onto the Concrete Docks](./images/052_yunchang_steps_onto_the_concrete_docks.png)

With renewed vigor, Yunchang is ready. She’s prepared to conquer the stagnant pools of biting fish, the muddy embankments, and the F6 stares at her exposed body.

Yunchang follows the call of nature ushering her deeper into the jungle. After passing an Isolated House built over the water, she finds her prey. The gators will suffer the wrath of her speartip until they spit up something of value.

![A Legendary Potion](./images/053_a_legendary_potion.png)

The gators cough up an Elixir! A potion of legend! While not incredibly useful yet, Yunchang knows to save these for the far off future.

There are only a few gators in this area of the swamp. To keep the balance of jungle life stable, Yunchang must also slay the White Roosters, Toads, and Yellow Lizards all around her. This is really going to slow her progress…

![Yunchang Is Fed Up with Poultry](./images/054_yunchang_is_fed_up_with_poultry.png)

She gets a couple Monster Book cards, but this area is not working out. After a short rest on top of Pooyai Lee’s home…

![Yunchang Eats Her Last Corn Stick](./images/055_yunchang_eats_her_last_corn_stick.png)

She heads back through the Floating Market, to the other far end of the swampy jungle. At the Toad Pond, Yunchang prepares to camp out for a long stay under the sun, palm trees, and bloodthirsty mosquitos.

This is not easy. Yunchang is on the gators’ home turf here, and needs to venture to the bottom of the pond to fight the gators! She holds her breath and does her best to keep swinging away at the many-toothed beasts.

![Underwater Ligators](./images/056_underwater_ligators.png)

To make matters worse, this place is also full of Toads and Jr. Neckis. Yunchang attempts to slay the agile snakes, but only one in a dozen hits even lands.

![Agile Jr. Neckis](./images/057_agile_jr_neckis.png)

A couple of hours pass. Yunchang fights along the shores, under the water, and atop the embankments. She holds her breath to fight the gators, and catches her breath while restoring her health on dry land. Just as things were looking truly grim…

![The Great Payoff](./images/058_the_great_payoff.png)

Holy buttfuck. Finally! A gator spits up an entire suit of armor before its eyes roll back into its head. Yunchang cleans the Yellow Engrit off in the water, dries it in the sun, and dons her first true piece of armor.

![Victory At Last for Yunchang](./images/059_victory_at_last_for_yunchang.png)

This Engrit is only 1 wdef below perfect! Yunchang wonders how its previous owner got eaten without sustaining any bite marks or tears on their armor, but shoves that thought aside. Yunchang is free now. Free to talk to any cute girl she wishes, buy anything she can afford, and try delicious goods from the food stalls.

![Finally, Only 1 Damage Per Hit](./images/060_finally_only_1_damage_per_hit.png)

She slays a few White Roosters just for fun, and gets ready to say goodbye to Thailand yet again.

![Goodbye Thailand, Hello Henesys](./images/061_goodbye_thailand_hello_henesys.png)

Her ship sails off towards the sunset, like an anime protagonist’s exit at the end of an episode. With this simple victory in the swamps, Yunchang can accomplish so much more. She can BE so much more.

------------------------------------------------------------------------

### Over the Shrooms and Far Away

After a long journey back to Maple World, Yunchang is tense. Her back aches for no fucking discernable reason, so she chalks it up to stress. Or maybe it’s her proximity to her old master, Dances with Balrog? Or the headache-inducing lightbulb above her head again?

In the Henesys town center, a kawaii neko girl sits alone on the bench, as if waiting for someone. God, she is just so fucking kawaii. Yunchang takes a seat next to the neko girl, and the bench creaks in anticipation of some awkward shit.

![Yunchang and CleverCookie](./images/062_yunchang_and_clevercookie.png)

**`CleverCookie`** shifts uncomfortably, and looks through Yunchang. This isn’t going anywhere. Yunchang is still spellbound by the cute neko girl, but has business to attend to.

The bunnies again shoot Yunchang straight up to the moon. With her moon bunny currency, she buys a couple of coupons.

![Purchasing the Lunar Cosmetics Bundle](./images/063_purchasing_the_lunar_cosmetics_bundle.png)

Yunchang is ready to redefine herself – as a warrior, as a woman, and as a God of War. There’s only way to do so, and that’s a fresh makeover.

She gets a magical haircut that actually causes her hair to grow. Wow!

![Waxing Moon Hair Obtained](./images/064_waxing_moon_hair_obtained.png)

Now, she’s got to set off for the continent of Ossyria. She walks to the Treetop Village, and at the end of the platform, a strange antennae-having cowgirl halts Yunchang.

“Passport?”

Oh, no. Yunchang never had her passport photo taken – is this why Cho the photographer had insisted Yunchang have her naked picture taken? Yunchang has no options left, so she bargains her way onto the ship with a direct threat of violence.

![Fuck the Maple Police](./images/065_fuck_the_maple_police.png)

Admittedly, Yunchang’s trip by airship was a beautiful one.

![Yunchang Rides the Airship](./images/066_yunchang_rides_the_airship.gif)

There was lots of room inside to stretch her legs – very much unlike the World Tour vessel.

![The Cozy Airship Cabin](./images/067_the_cozy_airship_cabin.png)

And an observation deck below the ship!

![Yunchang Feels the Wind in Her Face](./images/068_yunchang_feels_the_wind_in_her_face.gif)

The ship settles gently at the airship docks. What a delightful trip! Yunchang collects her belongings, and explores the City of Orbs.

The fairies here are not too kind towards humans, but Yunchang doesn’t mind. She’s here for just one thing, and it’s a new face.

![Yunchang Delivers the Coupon to the Face Dealer](./images/069_yunchang_delivers_the_coupon_to_the_face_dealer.png)

Looking good with that Curious Face, Yunchang! She wonders what Plastic Roy intends to do with her old face – perhaps recycle it into the pool of (REG) and (EXP) faces?

Yunchang returns to the airship, and is ready to head back to Victoria Island so she can return to the World Tour. This time, the ship is assaulted by an attack ship full of Crimson Balrogs! *AAAAAAH!*

![The Crimson Balrog Raid](./images/070_the_crimson_balrog_raid.gif)

Yunchang falls back into the cabin, letting a more seasoned warrior fight the Balrogs of Morgoth in her stead.

After the fight, he offers to let Yunchang scavenge their drops, but she must refuse the kind offer.

![Level 31 Status Update](./images/071_level_31_status_update.png)

Back in the Treetop Village, Yunchang knows it’s time to leave Maple World yet again. She coughs up 3,000 Mesos, mere pocket change at this point, and prepares for another journey on the rocky waves.

------------------------------------------------------------------------

### The Bounty of the Han

*APRIL FOOLS!* No Great Bounty of the Han this time around!

![Crappity Crap](./images/072_crappity_crap.gif)

Guys I’m too lazy, it’s 3 AM, and you basically saw everything Yunchang got. Don’t worry, chapter 5 is on the fucking way soon, my binches, and it’s gonna be fucking lit off the tit like always, and it’s coming SOON.

You can stop reading now if you want.

…What? *Still* not satisfied?

------------------------------------------------------------------------

![The Bounty of the Han: Dumped Into the Whangpoo! Oh no!](./images/073_the_bounty_of_the_han_dumped_into_the_whangpoo_oh_no.png)

Pretty cool, huh? Yeah no need to thank me, making this shit is its own reward.

**The Great Bounty**:

-   1,382,215 Mesos
-   8,750 Vote Cash
-   10 Common Gacha
-   4 Rare Gacha

------------------------------------------------------------------------

### Bromance of the Three Kingdoms, Part II

Check this shit out! On this [smelly retail Maple$tory site](https://maplestory.nexon.net/news/20210/cash-shop-update-1-18) (under Lunar New Year Permanent Equipment Covers) there are official™ face and hairstyles for those Three Kingdoms character costumes featured in Maple! (Here’s a link to the [actual full image](https://nxcache.nexon.net/umbraco/9744/lny782364r4.jpg) too.)

![Official Three Kingdoms Maple Characters!](./images/074_official_three_kingdoms_maple_characters.jpg)

I bet Diao Chan has Prudent Face, the best face in the game. Why didn’t they make a Lü Bu set though?

**New character alert!** The cast list continues to grow!

**Shu Han**

Rather fine folks that serve under Liu Bei to restore the Han Dynasty. Eventually Shu Han becomes an empire under him, but we try not to talk about that, sort of ruins the story.

![Sleeping Dragon](./images/075_sleeping_dragon.png)

-   Zhuge Liang (Kongming): A famed strategist known as Sleeping Dragon. Liu Bei travels thrice to his residence, meeting him on the third visit, thus forever gaining Zhuge’s trust and admiration. Smarter than me, and definitely smarter than you.

**Other**

Besides the actual Three Kingdoms people, there are some other relevantly irrelevant fuckers who sometimes pop up in the story.

![The Love Triangle](./images/076_the_love_triangle.png)

*From Left to Right*

-   Lü Bu (Fengxian): They try to make this guy the main character, but he’s too neurodivergent in a useless way, and basically is like a teenager mentally. Despite being a fully grown adult, keeps asking older men to adopt him because he MURDERED ALL OF HIS PAST ADOPTED FATHERS!!!!! Who the fuck would agree to adopt this guy? Basically his one and only talent bestowed upon him by Heaven is being really, really strong (and he’s got his girl Diao Chan, but she’s just playing both sides to come out on top). He’s got cute antennae at least. His neo-daddy Dong Zhuo was a piece of shit, so good on Lü Bu for killing that fuck. Why didn’t Lü Bu have to change his family name to Dong? It would have been a little funny. He also had a cool and SUPER FAST red horse called Red Hare (hare like a bunny, but it also literally has red hair too sometimes), and as the saying goes: Among men, Lü Bu; Among steeds, Red Hare.

-   Dong Zhuo (Zhongying): To explain Lü Bu, you’ve gotta explain the fat sex offender behind him. He did a lot of bad shit, and I’m not sure how much is actually relevant to whatever this diary is. This dude seized control of Luoyang, the capital at the time, replaced the emperor with a younger more puppety emperor (still Han though), had an alliance form against him, burned up the capital and ran away to Chang’an which he then made the NEW capital which was spicy I guess, and was known to never once wash his stinking privates. He probably took Diao Chan’s innocence, which is the reason Lü Bu sliced him up into bacon.

-   Diao Chan: Daughter of Wang Yun, a guy with a funny family name who was an old school Han dynasty minister man. She’s almost as kawaii as she is manipulative, because she basically tore up the whole shit with Dong Zhuo’s reign on purpose by courting both Dong Zhuo and his adopted son, Lü Bu. Diao-chan successfully enough follows through with her father’s plot to destroy the usurper. Did it save the Han dynasty? Uh…

A link to the full cast list can be found [here](https://codeberg.org/Taima/world_tour_diary/src/branch/main/bromance_of_the_three_kingdoms.md)!

------------------------------------------------------------------------

### An Interview with the Author, Part II

**Q:** An excerpt from Useless Dumb Shitter Mushroom’s comment,

“…What’s with the large animated GIF warnings? Merely a single megabyte? What is this, a GIF for ants??…”

**A:** Woah there! Those were some gigantic gifs if you ask me! You must be hooked right into the mainframe of the interwebs, buddy. I’m a huge fan of gifs! They just tie it all together nicely without feeling cold and webm-ey.

**Q:** More excepts from Useless Dumb Shitter Mushroom’s comments,

“En dashes (U+2013, –) aren’t used for breaks in sentences; that’s what em dashes (U+2014, —) are for.”

“Slight typo…”

“This should be ＜i＞, not ＜em＞…”

“Feel free to ignore this nitpick, but ＜strong＞ means that its contents are urgently important and must not be misread nor ignored… you want ＜b＞.”

**A:** I have a hard time differentiating between emphasis and italics, or strong and bold. My text editor also apparently has fucked me over and now I can only make things emphatic or strong. Why? What the hell happened to my italics and bold? HELP!

I think em dashes are fucking STUPID, they look EXTRA STUPID, and when I read the description of this crap on Wikipedia, it sure sounded like you could use an en dash for sentence breaks. I hate English. I’m just gonna commit to my broken process.

I’ve also decided to preserve any typos I make for future historians to study.

**Q:** An excerpt from Bort’s comment,

“＜em＞ is an additional 2 bytes… is not backwards compatible with all W3C html standards… is contextually equivalent to ＜i＞.”

**A:** Hey Bort, I don’t know why you’re getting all up in UDSM’s face. It seems you have it backwards, I’m actually the useless dumb shitter for using emphasis apparently. Help me make it italics I guess! Or not! It’s all the same to me!

Thanks for your awesome comments! Hope you all have a good poop!

![See You Next Time!](./images/077_see_you_next_time.png)

Bai, nee\~
