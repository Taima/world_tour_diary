![Chapter 2 Banner](./images/00_chapter_2_banner.png)

------------------------------------------------------------------------

## Chapter 2: Fear and Loathing in Thailand

Thought we blew our load in the first post, did ya? **NOPE**, Guan Yu (girl version) is **back again** for a new diary entry! Let’s get started!

### Along the River of Kings

Of the three nations that Spinel can bring us to, Thailand – also known historically as Siam – is the one that I know the least about! With some incredibly low-effort and uninformed Wikipedia, travel website, and online map digging, we’re going to figure out…

Where exactly is Yunchang now? And what is a “floating market” anyway?

Apparently, it’s a place where you’re supposed to paddle around in a small boat and buy fresh produce from other people… also in boats.

![Beep Beep! Heavy Traffic in Taling Chan Floating Market](./images/01_beep_beep_heavy_traffic_in_taling_chan_floating_market.png)

The floating market was a necessity of life in Siam for trade and survival, as most communities were built along the water. Floating markets had gone out of style for a long time, probably due to the lack of necessity thanks to industrialization and road building. Apparently, floating markets were absent from Bangkok until the late 1980s. Today, floating markets are a space for locals to preserve their traditional way of life, and are a booming tourist trap.

Let’s go ahead and guess that OUR Floating Market was one of those brought back in Bangkok after the late 1980s. The World Tour Floating Market has some signs of industrialization, from the ship Yunchang rode in on, to the tires scattered about, rotisserie, coolers, and of course the ***gigantic powered sign thing***.

![Who Is Funding This?](./images/02_who_is_funding_this.png)

There are many [floating markets](https://en.wikipedia.org/wiki/Floating_market) in Thailand, and only a few clues to guide our search. I’ll bet that someone more familiar with Thailand or who has visited Siam in ThailandMS may know better than me, and could correct me if I say anything stupid here!

The World Tour Floating Market features a walkable path through the wilderness directly to a walled Buddhist complex, known as the Golden Temple. I’ve come to think that the Golden Temple is a reference to [Wat Phra Kaew](https://en.wikipedia.org/wiki/Wat_Phra_Kaew), a Buddhist temple complex housing the Temple of the Emerald Buddha.

[This super reliable source](https://maplestory.fandom.com/wiki/Category:Golden_Temple) contains a small trivia piece which first led me to believe that the Golden Temple is based off of Wat Phra Kaew. This complex may be more accurately considered a chapel, as it does not have living quarters on site for the monks. The complex finished construction in 1784 and resides within the precincts of the Grand Palace in the historical center of Bangkok.

Let’s take a quick glance at a map of Bangkok, zoomed in on the Grand Palace!

![Map of the Grand Palace](./images/03_map_of_the_grand_palace.png)

Recognizable architectural features of Wat Phra Kaew that match with the Golden Temple are:

-   The easily recognizable Phra Si Rattana Chedi; a golden bell-shaped stupa inside of which is a round hall containing many relics given to Rama IV
-   Many giant guardian sculptures, one of which represents Ravana
-   Several lion statues of different styles
-   Low-lying surrounding walls with several entrances, though the colors do not match compared to our Golden Temple

Maybe we’ll show off some pictures of what these look like in real life in the far future, when Yunchang actually goes to the Golden Temple. For now, we’re just trying to create a reference point for the Floating Market’s possible location.

Given its walkable proximity to the center of Bangkok, our Floating Market is likely on the banks of the Chao Phraya River, one of its tributaries, or one of the many attached canals – this still doesn’t narrow the search much, given how huge the Chao Phraya River Basin really is.

There are many likely candidates, but I’ve chosen the two closest floating markets based on online maps for further consideration. Huge warning that this info is very imperfect due to my ignorance, I’m only using English resources, and the online maps used probably are not that accurate.

Bangkok Noi Water Market, located on [Khlong Bangkok Noi](https://en.wikipedia.org/wiki/Khlong_Bangkok_Noi) (“Small Bangkok Canal”). Very close to Wat Phra Kaew!

-   4.3km (53 minute) walk from Wat Phra Kaew
-   2.5km (29 minute) combined ferry and walking travel

![Walking to Bangkok Noi Water Market](./images/04_walking_to_bangkok_noi_water_market.png)

[Taling Chan](https://en.wikipedia.org/wiki/Taling_Chan_district) Floating Market, located on [Khlong Chak Phra](https://en.wikipedia.org/wiki/Khlong_Chak_Phra) (“pulling the Buddha canal,” named after an annual [Buddhist celebration](https://en.wikipedia.org/wiki/Chak_Phra)).

-   7.8km (97 minute) walk from Wat Phra Kaew
-   6.5km (78 minute) combined ferry and walking travel

![Walking to Taling Chan Floating Market](./images/05_walking_to_taling_chan_floating_market.png)

It seems that part of the tourist appeal of Bangkok Noi Water Market lies in the several close-by temple sites, which were built ranging from 200 years before to a few decades after the construction of Wat Phra Kaew. But these temples aren’t visible in the World Tour Thailand area at all. It’s also a bit harder to dig up info on this floating market compared to the next candidate, so let’s keep looking just for fun.

Taling Chan Floating Market is open on weekends and public holidays only, from 8:30 to 16:00. From 11:00 to 14:00, there is a live traditional Thai music performance – kind of like [this](https://www.youtube.com/watch?v=p_8Pobw0d4I). Pretty reminiscent of Thailand’s background music, isn’t it? Fish, fruits, and vegetables are sold here from small boats on the canal. 1,000 years ago, the landscape of current-day Taling Chan district was a muddy mangrove forest, with no evidence of human settlement. This sounds a bit like the jungle forest areas outside of our Floating Market – not counting the human settlement!

Let’s be honest: neither of these floating markets’ **extremely industrialized** surroundings match that of the World Tour Floating Market. This was just a fun exercise. I’ll try to give other World Tour areas the same treatment, keeping in mind that these are all miniatures of real life areas, some of which are a slop of different time periods cobbled together right next to each other.

------------------------------------------------------------------------

### Guan vs. Wild

Yunchang is not welcome here.

The locals usually accept tourists with open arms, and offer a welcoming ritual for all to partake in. But Yunchang, unclothed and fully embracing the tropical climate, is met with stares of bewilderment, parents covering their childrens’ eyes, and at best, repetitive blinking from those with broken animations.

![Pond and the Repetitive Blink](./images/06_pond_and_the_repetitive_blink.gif)

Besides, Yunchang is without any Mesos to support herself. She continues by the local food sellers on the waterside.

As the pier lowers into the swamp, Yunchang splashes in the water for a moment, before feeling a sharp bite.

![Danger in the Water](./images/07_danger_in_the_water.png)

Within the waters of the Brilliant Swamp are several freshwater varieties of pissed off fish. I’d want to ignorantly call these pirahnas, but pirahnas inhabit South America, and are rarely found elsewhere unless artificially introduced. What even are some of these, some variety of barracuda?

Yunchang receives a message from the Maple Administrator, yet another supernatural being, this one capable of transmitting messages far and wide. She hears the voice of her old master, Dances with Balrog – gaslighting her, playing down her abilities. Yunchang feels a renewed sense of vigor and begins training. Hard.

![Yunchang Proves Her Old Master Wrong](./images/08_yunchang_proves_her_old_master_wrong.png)

The frogs of Thailand seem to have **razor sharp teeth** with a nasty bite. Yunchang takes about four times the damage from the frogs as she does from the not-pirahnas! However, she has no choice but to keep fighting for survival, hoping to score *SOMETHING* to wear. She bites into some chewy raw frog legs, and continues on.

Finally having recovered enough strength, she can comfortably wipe out the chickens on the muddy embankment as well. Finally, something else to eat.

Yunchang scores a sweet Wooden Baseball Bat! She tucks away her plastic toy sword and is ready to smack away at some more wildlife.

![Yunchang Finds a Wooden Baseball Bat](./images/09_yunchang_finds_a_wooden_baseball_bat.png)

She finds a pair of shoes from the saber-toothed froggers, but they aren’t her size. Useless! She stashes them away to sell later. While snacking on some raw chicken, a renowned legend of Maple World approaches, sporting a Tier-10 Ring glimmering with several kinds of gems.

After laughing at her predicament, **`Cortical`** promises to spread word of Yunchang’s misfortune across the world.

![Meeting with a Renowned Hero](./images/10_meeting_with_a_renowned_hero.png)

Cortical engages Yunchang in a battle of wits. The two stare attentively at the Match Cards as they are mixed and shuffled.

![A Battle of Minds](./images/11_a_battle_of_minds.png)

Yunchang wins the first bout! She becomes overconfident, and forgets to watch the Match Cards as closely this time around.

![The Battle of Minds Ends in Disgrace](./images/12_the_battle_of_minds_ends_in_disgrace.png)

Yunchang admits her overconfidence and – reflecting on her circumstances – understands that she doesn’t exactly appear trustworthy. She accepts that it will take some time to earn back her fame and honor. Cortical slays several chickens with just one slash each, and then parts ways with our hero.

Alone again, Yunchang feels an unpleasant stir inside her stomach. Oh no.

![Queasy Bar 92%](./images/13_queasy_bar_92_percent.png)

She decides to find a spot to settle down and get some water from a source that doesn’t harbor any not-pirahnas. Taking a trip back through town, she is no more welcome now than before. She reaches a Frog Pond, populated by the same variety of deadly frogs. After slaying several and collecting their froggy legs, she gulps a bit of fresh water and bathes.

![A Freshwater Bath](./images/14_a_freshwater_bath.png)

The raw poultry Yunchang ate earlier begins to take its toll. She experiences excruciating pain in her midsection, and isn’t sure which end she’ll release a deluge of disgust from first. The Salmonella poisoning takes hold.

![Yunchang Contracts Salmonella](./images/15_yunchang_contracts_salmonella.png)

A colorful spew of vomit emits from Yunchang, and refuses to let up until she feels much weaker and lighter.

The long day of survivalism is at an end, and Yunchang has nothing left to give in pursuit of her goals. She retires for the evening under some light foliage, keeping her Baseball Bat handy in case of frog attacks.

![A Hero Is Always Vigilant](./images/16_a_hero_is_always_vigilant.png)

Yunchang awakes to the hot bake of the tropical sun, but still feels drained. After cleaning the crusty vomit off of her undergarments and face, she does her best to eat a few bites of raw frog legs, and drafts a new plan.

After beating back the chickens blocking her path back to the Floating Market, she finally arrives back in town. She barely notices the stares at her exposed body. Approaching a man blinking repetitively, probably out of pure fury, she asks to use his rotisserie. He grants Yunchang no response.

She’s fed up with it all, and hands the man some raw ingredients that she’s stashed up until now. Frog legs, eggs, and chicken. What the…?

![Yunchang Completes the Cook’s Assistant](./images/17_yunchang_completes_the_cooks_assistant.png)

The man blinks in acceptance of the ingredients, and sets to work on… a cake? Whatever, man. Yunchang fires up the rotisserie and gets cooking, knowing that her constitution cannot afford another bout of stomach splurging.

![Yunchang Trains Her Cooking Stat](./images/18_yunchang_trains_her_cooking_stat.png)

As Yunchang works the rotisserie, she progressively burns less and less chicken. Satisfied with her cooking, she eats her freshly seared chicken and smiles with pride.

Looking up at the weapon stall above her, Yunchang notices a familiar weapon! It looks just like the polearm she’s spent years training with.

![Yunchang Spots the Guandao for Sale](./images/19_yunchang_spots_the_guandao_for_sale.png)

The weapon salesman eyes her with suspicion, so she backs off. Regardless, it’s probably just another replica Guandao, and she can’t yet afford a brand new weapon anyway.

She must return to her scavenging in the wilderness. She prepares for battle with the frog and chicken menace yet again.

![Yet Another Training Montage](./images/20_yet_another_training_montage.png)

And a training montage ensued.

![Level 17 Status Update](./images/21_level_17_status_update.png)

Back in the Brilliant Swamp, Yunchang inspects the stronger variety of chickens, and begins to slaughter them for their feet. With proper footwork in her fighting stance, she can reach across the gaps in the muddy earth and avoid their fierce pecking.

![Thailand Hunting Ground I](./images/22_thailand_hunting_ground_i.png)

The training drags on and on as Yunchang collects body parts and swallowed Mesos from the wildlife. Until she spies from the rubble of the many torn frog carcasses…

A new weapon! This one made of metal!

![Yunchang Acquires the Metal Axe](./images/23_yunchang_acquires_the_metal_axe.png)

Plastic, to wood, to metal. Such progress! This Metal Axe will serve Yunchang well.

![Level 19 Status Update](./images/24_level_19_status_update.png)

While successful in her wilderness survival efforts, Yunchang is still naked. And beneath that, she feels an emotional nakedness, as she grows homesick. She returns to town, and asks Spinel for assistance.

![Spinel’s Postcard Service](./images/25_spinels_postcard_service.png)

The World Tour stand offers a postcard delivery service, with many varieties of themed postcards for sale. Yunchang can’t stop thinking of her brothers, and pays Spinel a bit of Mesos to send a message back home. With tears in her eyes, she picks a most fitting postcard, and writes a heartfelt message.

![A Postcard Back Home](./images/26_a_postcard_back_home.png)

Yunchang wipes her tears away, feeling a bit better about her situation. It will be a long time before she can make it home to see her sworn brothers or her home.

Yunchang continues her wilderness training for just a bit longer! A super strong chicken coughs up the first card Yunchang is allowed to pick up!

![Yunchang Obtains Her First Card](./images/27_yunchang_obtains_her_first_card.png)

And finally, the fierce frogs hand over a White Bandana! Yunchang is clothed, or at least, sort of.

![White Bandana Plus Meso Count Update](./images/28_white_bandana_plus_meso_count_update.png)

Yunchang decides that she’s had enough of the unforgiving swamp, as well as its unforgiving inhabitants. She approaches the World Tour stall again, and Spinel nods as they make eye contact. Before she boards the ship for a new adventure, Yunchang glances at her supplies.

![One Last Inventory Check](./images/31_one_last_inventory_check.png)

She pays the 3000 Meso troll toll, and looks off to the horizon. Things can only get better from here, she thinks.

------------------------------------------------------------------------

### The Bounty of the Han

In this all new section of **GUAN YUNCHANG’S WORLD TOUR DIARY**, we’ll be taking a look at the bounties bestowed upon Yunchang throughout her journey! Take note that this does not include fresh equipment that Yunchang will use, monster cards, or any big ticket items in general that are deserving of their own showcase in the main diary.

![The Bounty of the Han](./images/29_the_bounty_of_the_han.png)

Woah, that’s a lot of NX cards! As well as her very first Gachapon ticket! Keep it up, Yunchang!

![Bountiful NX](./images/30_bountiful_nx.png)

2000 NX! It’s really starting to add up now. Yunchang will keep saving up her NX and Gachapon tickets. After all, she can’t use the Gachapon until after level 30.

Tune in next time! Let’s all wish Yunchang good luck finding some clothes!

------------------------------------------------------------------------

## Twin Fantasy

Can’t get enough MapleStory area-lock content? Desperate to see the adventures of others partaking in a similar **World Tour Challenge**? Let’s compile all of these challenges into a single spot for convenient clicking.

### World Tour Challenge

Explore the real world from the comfort of MapleStory!

-   [World Tour Lock Resources](https://codeberg.org/Taima/world_tour) (Super stinky self-shill)
-   ([MapleRoyals](https://zombo.com)) [Game Challenge: World Tour™](https://mapleroyals.com/forum/threads/game-challenge-world-tour%E2%84%A2.209996/) (Completely unaffiliated with me)

### Other Area-Locks on MapleLegends

A mix of community threads, guild advertisements, guides, and rulesets. A rich bounty of area-lock!

-   [Victoria: The Victoria-Island Locked Guild!](https://forum.maplelegends.com/index.php?threads/victoria-the-victoria-island-locked-guild.41605/) and [A guide to vicloc](https://codeberg.org/Victoria/resources/src/branch/master/guide.md)
-   [Osslock (Old School Style Lock)](https://forum.maplelegends.com/index.php?threads/osslock-old-school-style-lock.46610/)
-   [Islander Community Thread](https://forum.maplelegends.com/index.php?threads/islander-community-thread.5922/)
-   [Sleepywood-locked Challenge](https://forum.maplelegends.com/index.php?threads/sleepywood-locked-challenge.29843/)
-   [Clocklock rules](https://codeberg.org/deer/clocklock/src/branch/main/rules.md)

### Other Area-Locks on [MapleRoyals](https://zombo.com) (I Don’t Play MapleRoyals)

I don’t know these people, but I’m glad they are having fun.

-   [Camper Community Guide / Ranks](https://mapleroyals.com/forum/threads/camper-community-guide-ranks.30157/)
-   [Islander Community Rankings / Guides](https://mapleroyals.com/forum/threads/islander-community-rankings-guides.17081/)
-   [Game Challenge: Caveman™](https://mapleroyals.com/forum/threads/game-challenge-caveman%E2%84%A2.120717/)
-   [Game Challenge: LEGOman \[Ludibrium only challenge\]](https://mapleroyals.com/forum/threads/game-challenge-legoman-ludibrium-only-challenge.149165/)
-   [Game Challenge Ellin Guardians](https://mapleroyals.com/forum/threads/game-challenge-ellin-guardians.213368/)

Bai, nee\~
