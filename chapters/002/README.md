![Chapter 3 Banner](./images/00_chapter_3_banner.png)

------------------------------------------------------------------------

## Chapter 3: The Traitors of the Southlands

Hey again! If you’re reading this still, I hope you’ve enjoyed the first two chapters of **Guan Yunchang’s World Tour Diary**! I wonder what I’m even doing writing this at all, but the server is in maintenance AGAIN, so I’m going to keep writing. Now it’s time for rabbits on the moon, fuck yeah! I don’t plan on stepping away from the computer for even a second during maintenance. Watch in real time as I learn how to edit gifs and use basic software I could have learned 10 years ago! Let’s put this awful thing to paper already.

![Excited Yunchang’s Diary Fans](./images/01_excited_yunchangs_diary_fans.gif)

### Youkoso! Kinoko Jinja!

Time for a long, drawn out chapter explaining what a Shinto shrine is. Maybe we’ll look at some local Japanese maps and reference the trajectory of the camera in MapleStory *looking at* Mt. Fuji versus where Mt. Fuji really is, and see if there are any Shinto shrines within a wide, sweeping area! You see, Shinto is often referred to as an animist religion, but its practitioners would not exactly call it a religion. The Shinto shrine is a place where…

Nah, just kidding! Let’s get to the story.

------------------------------------------------------------------------

### ようこそ！キノコ神社！

Yunchang awakens in the dark ship cabin, swallowing and gasping for air, holding back her seasickness. By candelight, she grabs a World Tour Map from the table and adds some personalized touches.

![The World According to Yunchang](./images/02_the_world_according_to_yunchang.png)

Many nights passed as the Maple Travel Agency’s metal ship, clearly made in like 1890 or something, peeled apart the ocean waves. Conversation was sparse on the rusty vessel. Its inhabitants, both crew and traveller, felt instinctively that with one false move a screw or support would come loose, dooming them all to the ever-awaiting abyss below.

Yunchang’s uncertainty about the approaching landmass mirrors the depth of the abyss, and she feels herself swallowed by the darkness.

![The World Tour Vessel Makes Ground](./images/03_the_world_tour_vessel_makes_ground.png)

As the ship ploughs to a stop against the shore and emits its last cough of steam, Yunchang rushes to gather her belongings and plant her bare feet on the unfamiliar shore.

*Grumble grumble*

![Poopoo Bar 86%](./images/04_poopoo_bar_86_percent.png)

Youkoso… Kinoko Jinja. Welcome to… Mushroom Shrine? Kerning shitty… under construction? Oh, wait, that’s CITY, not shitty. Darn katakana loan words. Yunchang should really study her Japanese flash cards…

![Mushroom Shrine Message Board](./images/05_mushroom_shrine_message_board.png)

Wishing to respect local custom, Yunchang believes she should bow before passing through the great torii leading her to the Mushroom Shrine. She’s not sure if you’re supposed to just bow once or at every torii she sees, but this one is pretty big, so bow she does.

![Bowing Before the Torii](./images/06_bowing_before_the_torii.png)

The Maple Travel Agency ship (welded from scrap steel in 1890) did not have a bathroom save for a lone bucket, so Yunchang has really got to go. Naked, she quickly hops one foot at a time to the nearest bush, and gets to business.

![First Poop in Japan](./images/07_first_poop_in_japan.png)

Deeper Yunchang goes into the Mushroom Forest. Just as the shrine disappears from behind her, the scenery bursts to life before her eyes. The mushrooms are up, they are walking, and they want blood.

![The Mushroom Forest Guardians](./images/08_the_mushroom_forest_guardians.png)

Clearly, these animated mushrooms have been harassing travellers along the road, and stealing their goods. A perfect opportunity for heroes like Yunchang to make a name for themselves.

![First Orange Mushroom Card](./images/09_first_orange_mushroom_card.png)

What luck! Yunchang gets an Orange Mushroom card, and is actually able to pick it up, too!

The mushrooms have fashioned some common household objects into weaponry and armor. Yunchang takes the Pan Lid, and takes a hint from the mushrooms by putting it in her Equip inventory, not her Etc. inventory.

![Pansexuals Rejoice!](./images/10_pansexuals_rejoice.png)

A long time passes as Yunchang sweeps the forest of living fungi. Cards montages were had…

![A Splendid Card Gathering Montage](./images/11_a_splendid_card_gathering_montage.png)

And a few level ups, and lots of junk items to sell. But you’ll have to wait until the **Bounty of the Han** to see the great compilation!

Yunchang begins to encounter female mushroom troops, suited up in female armor. Unfortunately, none of that armor is for female WARRIORS…

![Useless Female Armor Drop](./images/12_useless_female_armor_drop.png)

Yunchang turns over a bloodied mushroom girl and finds a Yellow Valentine Rose. These high quality roses are only found in Amoria, or shared between lovers for certain commercialized holidays.

![First Yellow Valentine Rose Drop](./images/13_first_yellow_valentine_rose_drop.png)

Yunchang buries the mushroom girl and says a small prayer. She knows that the mushrooms do what they must out of poor circumstance, not for the sake of causing chaos.

The battle continues.

As Yunchang dives to escape the speartip of a determined Blue Mushroom, she feels an object sink effortlessly into her back. Her breath stops, and the tendrils of pain seek out every corner of her body.

The pain stops. She feels lighter. As the wind blows, she feels a bit of her body drift away. What… what happened?

![An Adventure Cut Short](./images/14_an_adventure_cut_short.png)

She reaches out to feel anything, but her hands stay limp. After floating for a moment, the world grows dark.

------------------------------------------------------------------------

### The Traitors of the Southlands

Yunchang wakes up, but the world is still black. A muffled voice speaks excitedly. She hears the sound of hands pawing at some surface in front of her, until a latch pops open. Still in darkness, Yunchang can only feel the plush surface beneath her, carrying her off to the unknown.

The undignified man loudly clears his throat and practices some lines. “Lord Sun Ce – wait, I mean Lord Sun Quan! I present this gift, from yours truly.”

Stopping to greet another presence, the undignified man puffs out his chest and barks at someone of inferior station, “Lü Meng. Carry our gift, and follow behind me as we present it to Lord Sun Quan!”

Entering the chamber with echoing footsteps, the two bow in respect, and Zhou Yu begins to speak.

“Lord Sun Jian – oh, damn it – Lord Sun Quan. I pring this blesent – fuck, BRING this PRESENT… Oh, damn it already, it’s the head of Guan Yu! Gaze in wonder, my Lord.”

![Zhou Yu and Lü Meng present Guan Yu’s head to Sun Ce – fuck, I mean Sun QUAN. God, there are too many names here](./images/15_zhou_yu_and_lu_meng_present_guan_yus_head_to_sun_ce_fuck_i_mean_sun_quan_god_there_are_too_many_names_here.png)

Sun Quan puts away a McDonalds wrapper, the packaging crumpling loudly in his hands. He grabs a fresh burger from the pile to his side and rejoices.

“Yes! Well done, Zhou Yu, and you too Lü Meng I guess! Yunchang doesn’t look like much of a God of War – just another plain warrior, obeying the words of another. Still, we can now retake Jing Province.”

“Already underway, my Lord,” says Zhou Yu, looking past Sun Quan.

“Anyway, Cao Cao would love to see this. Send him this head in a jeweled box, with a procession of 10,000 McNugget carts. And give him a few thousand horses, generals in this era love that shit. With this, our alliance with Cao Wei is secured, and definitely won’t blow up in our faces immediately after.”

Zhou Yu and Lü Meng come to attention, complete a fist and palm salute, and exit the chamber, their armor clanking obnoxiously through the halls.

“Da Qiao, Xiao Qiao, did I tell you to stop? Keep going!”

“Hai, Goshuujin-sama,” the Qiao sisters say in unison, and also in Japanese for some reason. Tears dripping down their faces, they each unwrap another McDouble, and begin to take delicate bites.

As the Qiao sisters get busy, Sun Quan picks up the pillow and strokes Yunchang’s hair. He begins to hum, or moan, or… something.

Yunchang opens her eyes and comes face to face with Sun Quan. The smell of burgers and stale sweat is too much, the traditional Sun family clown makeup too terrifying. She screams for help that she knows isn’t coming. She screams out for Liu Bei, for Zhang Fei that hungover lunatic even, and for the Great Han’s twenty four emperors.

![Yunchang’s Scream of Desperation](./images/16_yunchangs_scream_of_desperation.png)

She spits a frothing mess of blood, and Sun Quan drops the decorative pillow. Rather than hitting the floor, Yunchang crashes out of this world, and into the next.

------------------------------------------------------------------------

### The Mystery of Mushroom Shrine

Yunchang rests on manicured grass. The snowshoes and boots of adventurers walk past her, going either way, ignoring her presence. The Gachapon machine squeals in delight while burping up goodies, but the adventurers mostly just sigh and drop their newfound garbage onto the grass. A shrine maiden with poor social skills begins sweeping away the Gachapon detritus from the grass, while humming a song to herself. Yunchang glances up and wonders why this girl must sweep the grass. It’s probably some old school training thing meant to build discipline, she guesses.

![Many Offerings Are Strewn About the Shrine Grounds](./images/17_many_offerings_are_strewn_about_the_shrine_grounds.png)

The shrine maiden looks at Yunchang, embarrassed to have been caught humming to herself. She attempts to ask if Yunchang needs help, or something to wear. “Etto… etto…” The shrine maiden looks away, hiding her face, trying again to collect her thoughts.

Oh man, this is awkward. Yunchang hasn’t seen this variety of unsocialized girl since anime club back in diaper school. “Look, it’s fine,” Yunchang sighs, “I’ll step off the shrine grounds. Just please go relax or something.” Yunchang climbs a tree which is somehow the exit to this place, glances in sadness at her EXP bar, and prepares for more mushroom murder.

![Sad Post-Death Yunchang](./images/18_sad_post_death_yunchang.png)

Mushroom caps crack open, and spores fly out, gently falling back to the ground. Observing the cycle of life and death or something like that, Yunchang ponders… Perhaps the blessings of Mushroom Shrine revive both humans and mushrooms alike.

Yunchang gets a Bronze Koif! Wow, she’s looking like a true Warrior!

![Bronze Koif Loot](./images/19_bronze_koif_loot.png)

She dies a whole bunch of times, and is revived by the magic of Mushroom Shrine. What’s doing it? Could it be the offerings to Mushroom Shrine? Why do the shrine boys also collect these offerings? What are they planning?

![Shrine Boy Hard at Work](./images/20_shrine_boy_hard_at_work.png)

Yunchang accepts the free revival, but hopes that no human sacrifice or weird shit is at play.

More EXP, more levels. The deaths won’t stop Lady Yunchang! And among the shroom wreckage lays… an Iron Axe! What an upgrade!

![37 Att Iron Axe PLUS 3rd Blue Mushroom Card](./images/21_37_att_iron_axe_plus_3rd_blue_mushrooom_card.png)

Many Monster Cards are collected, both cards for the Monster Card book and many Match Cards. The Monster Book tells her which mushroom troop varieties are equipped with which items. Yunchang, desperate for clothing, begins to focus on the Blue Mushrooms, hoping to get a [Steel Fitted Mail](https://maplelegends.com/lib/equip?id=1051000)!

Many adventurers pass through the forest. They must be much stronger than Yunchang, as they react little to the battalions of mushrooms, instead rushing to pray at the Great Mushroom Statue. One stops for a moment and offers our hero fresh supplies. However, Yunchang cannot accept in good conscience.

![A Chat With BlueCrawfish](./images/22_a_chat_with_bluecrawfish.png)

Yunchang gets some more extremely cool items, but nothing meant for a Warrior. She develops a plan…

Remember, the shopkeepers will never sell items to Yunchang until she is appropriately clothed! The climate may be fair enough to prance around nude, but there are social standards in place.

Rather than equipping some armor, why not armor her body? It’s only temporary, but…

![Iron Bodied](./images/23_iron_bodied.gif)

With this trick, Yunchang can create a barrier of iron around her body. It only lasts for a couple minutes. While this **isn’t long enough** to be *served food* by the local vendors, it’s just long enough to *sell some items* from her overflowing backpack. Nice loophole! Get those Mesos, Yunchang!

![Food Safety Be Damned](./images/24_food_safety_be_damned.png)

Yunchang prances back into the forest before her Iron Body disappears, and again begins scavenging from the mushroom menace.

What’s this, a Bullet? Yunchang had heard of this technology, but had never laid her eyes on such a thing. She’d thought the ashigaru were still using pointy stick weapons, not advanced projectile weaponry. Who’s producing these in Japan?

![Never Mind the Bullets, Here’s the Sex](./images/25_never_mind_the_bullets_heres_the_sex.png)

Moooore levels and moooore offerings! This time, Yunchang discovers that adventurers offer items not just to Mushroom Shrine directly, but also to the Great Mushroom Statue! There’s got to be something special about this Mushroom.

![Offerings to the Great Mushroom Statue](./images/26_offerings_to_the_great_mushroom_statue.png)

Yunchang finally has 50 Monster Cards (for Match Cards)! Now she can make a set of Match Cards, but that would require a trip back to Maple World. Dreadful place.

![Yunchang Loots Her 50th Monster Card](./images/27_yunchang_loots_her_50th_monster_card.png)

Yunchang occasionally returns to sit on the bench and recover some energy. While she does have some potions and Bento stashed away, she prefers to save them, feasting on mushroom caps and leftover frog legs instead. During her mealtime bench sitting, the shrine boys aren’t very talkative.

![Shrine Boy Update](./images/28_shrine_boy_update.png)

Perhaps Yunchang should start using some of her supplies. A bit deeper into the forest she goes\~

And in the clearing are many crows\~

Will they drop a [Red Engrit](https://maplelegends.com/lib/equip?id=1051011), nobody knows\~

Prepare for a gigantic filesize gif. I’m truly sorry about this.

![The Battle of the Crows](./images/29_the_battle_of_the_crows.gif)

These Crows are TOUGH! Yunchang must constantly return back to the Mushroom Forest to sit in place healing.

![A Perfect Time to Eat Frog Legs](./images/30_a_perfect_time_to_eat_frog_legs.gif)

Yunchang spots a lone girl at the Mushroom Shrine. Feeling the weight of her backpack, Yunchang offers one of her spare Yellow Valentine Roses. It’s really the least she could do, and besides, she’s just a Beginner.

![A Rose for Taima](./images/31_a_rose_for_taima.png)

Even more lovely offerings were left at the Mushroom Shrine. Please, keep offering! This is what sustains us lesser adventurers!

![Funky Offerings](./images/32_funky_offerings.png)

Back to crows…

and back to mushrooms (ridiculously large gif alert)…

![Delicious Mushroom Mobbing](./images/33_delicious_mushroom_mobbing.gif)

Hey, a random fame! Word of Yunchang’s heroics must be spreading!

![Yunchang Receives a Fame](./images/34_yunchang_receives_a_fame.png)

to crows…

![A Brief Card Montage Is in Order](./images/35_a_brief_card_montage_is_in_order.png)

to mushrooms…

![Level 28 Status Update Featuring New Axe](./images/36_level_28_status_update_featuring_a_new_axe.png)

to crows, to mushrooms. And endless cycle, occasionally broken by death and resurrection, which is also a cycle.

After one of the several deaths and resurrections, Yunchang meets the brilliant Ranger **`Celestalia`** at the Mushroom Shrine! Yunchang decides not to share any excerpts from this one in the diary. The two shared a chat for a brief moment, and I’m sure their paths will cross again.

Yunchang continues struggling to get a SINGLE PIECE OF ARMOR! AND DIES AGAIN! AND AGAIN!

DROP IT ALREADY YOU MUSHROOMS! YOU PIECE OF SHIT, YOU HAVE A SLIGHT CHANCE TO DROP IT, YOU CAN BE THE MUSHROOM WHO SAVES YOUR KIND FROM THE SLAUGHTER, YOU CAN DROP THE ARMOR PIECE, BUT AGAIN YOU FUCKING DIDN’T!

![Useless Dumb Shitter Mushroom](./images/37_useless_dumb_shitter_mushroom.png)

She tries a couple different fighting styles – broadening her reach, wielding a shield. None of it helps. In the confusion between battle and AFKing, she lets a NX card disintegrate. But not before screenshotting it!

![The NX that Got Away](./images/38_the_nx_that_got_away.png)

Little more offerings to the Mushroom Shrine.

![Tiny Offerings](./images/39_tiny_offerings.png)

Shrine boy reports back to headquarters. Oh, something big is going on here.

![Shrine Boy Reports Back](./images/40_shrine_boy_reports_back.png)

Yunchang gets back to the grind, hitting level 29!

![Mushroom Explosion](./images/41_mushroom_explosion.gif)

FUCK YES, VICTORY FOR ONCE! Yunchang scores something to wear!

![Yunchang Obtains the Mithril Boots](./images/42_yunchang_obtains_the_mithril_boots.png)

Some boots, and they’re made of [mithril](https://en.wikipedia.org/wiki/Mithril), a superb material! Thanks, Tolkien! Along with a useless Iron Axe! It’s not enough to function in modern society, but it’s enough to keep her feet protected during this long march through the woods. She notices that her steps through the forest now leave much more noticeable tracks behind her, but she doesn’t fucking care even a little, she’s getting that weapon defense.

![Level 29 Status Update](./images/43_level_29_status_update.png)

A little trip back to the Shrine, a little Iron Body, and a little selling to the locals. She spots the Shrine Boy! Go Shrine Boy!

![SHRINE BOY](./images/44_shrine_boy.png)

Yunchang spots an adventurer gliding along in a mechsuit! Wow, cool! He warns her of the approaching end times.

![Yunchang Meets Mechsuit Man](./images/45_yunchang_meets_mechsuit_man.png)

She wonders if it’s a real high tech mechsuit, or if this is some cosplayer from Shibuya. Regardless, pretty awesome.

The world prepares for a deep sleep. Yunchang is ready for some sleep, too. She takes inventory before the great light in the sky goes out for a bit.

![Almost Level 30 Status Update](./images/46_almost_level_30_status_update.png)

But Yunchang, despite wanting to sleep peacefully…

![A Shadow Approaches](./images/47_a_shadow_approaches.png)

can’t help the feeling that someone is watching her.

![Unhappy Guan Yu](./images/48_unhappy_guan_yu.png)

------------------------------------------------------------------------

### THE BOUNTY OF THE HAN

Biggest haul yet. Bring it on home, Liu Bei. Really, really obnoxious image alert.

![The Bounty of the Han](./images/49_the_bounty_of_the_han.png)

**The Great Bounty** thus far:

-   9 Common Gacha
-   3 Rare Gacha
-   453,719 Mesos
-   8,100 Vote Cash

![The Great Levelup Montage](./images/50_the_great_levelup_montage.png)

Glorious.

![The Great Death Tally](./images/51_the_great_death_tally.png)

Not so glorious – that’s a LOT of deaths before level 30. But I’m actually a little proud of how the Death Tally image turned out. I’ll see if I want to keep making such obnoxious compilations in the future, because this was a lot of work.

Feel free to stop reading now if you just like MapleStory. I don’t know why I’m doing this silly plot thing.

------------------------------------------------------------------------

### The Bromance of the Three Kingdoms

This is going to get confusing, so let’s do one of these cast listing things, like for a play I guess? The names begin with the family name, then their personal name, followed by their style name in parentheses. Women don’t get style names, apparently. If I’m doing this thing wrong or in an offensive manner, let me know! This bastardization of the Three Kingdoms characters and plot is definitely not the worst of what’s out there, I assure you. I’m probably doing a ton of shit wrong, but I’m having fun at least.

Characters not yet featured in the story will have illustrations for their pictures (or whatever image I can find), but I’ll swap those out for character pictures when they are later revealed. Kinda like unlocking a new character in a fighting game or something?

Since these are historical figures and are featured in one of the [great classical novels](https://en.wikipedia.org/wiki/Classic_Chinese_Novels) of Chinese literature, I’d like to stress that this is a parody and all in good fun. I come from a region where nobody knows what a Three Kingdoms even is! Honestly I can’t fucking explain this Three Kingdoms thing, so I’ll just hope literally a single person is able to understand what’s going on.

**Shu Han and the Brothers Liu**

Rather fine folks that serve under Liu Bei to restore the Han Dynasty. Eventually Shu Han becomes an empire under him, but we try not to talk about that, sort of ruins the story.

![The Brotherhood of the Mushroom Garden](./images/52_the_brotherhood_of_the_mushroom_garden.png)

*From Left to Right*

-   Guan Yu (Yunchang): Our main man, who has somehow become a girl, and you’re just gonna deal with it. Why would anyone play a male character on MapleStory anyway? For some reason they always depict Guan Yu with red skin all over like a high school football coach. Has a sense of honor as strong and heavy as iron.
-   Liu Bei (Xuande): The star of the show (except for Guan Yu); the man’s got big earlobes and an even bigger sense of filial piety. A scion of the Han, who would do anything to protect the people.
-   Zhang Fei (Yide): The youngest brother, often depicted as a powerful, disheveled, bearded man. Really likes drinking, and wields an epic serpent spear.

![This Is Also Guan Yu](./images/53_this_is_also_guan_yu.png)

There are more of these Shu Han guys, I just can’t figure out how to write them in yet. Give me some time, okay?

**Sun Wu**

Irrelevant bastards, whenever you reach a scene with these guys just skip it honestly.

-   Sun Quan (Zhongmou): Baby boy Sun, forced to come to power after his father and big brother died. How did they die? Idk, haven’t written it yet.

![Sun Quan](./images/54_sun_quan.png)

-   Sun Ce (Bofu): Bigger boy Sun, known as the Little McConqueror after he took a bunch of land or some shit. He probably smokes weed or surfs.

![Sun Ce](./images/55_sun_ce.png)

-   Sun Jian (Wentai): Big daddy Sun. The Tiger of Jiangdong. Over 99 billion served, and counting. He got done dirty early in the story by dying after he got the Imperial Seal, and his sons kind of suck, don’t they?

![Sun Jian](./images/56_sun_jian.png)

-   Zhou Yu (Gongjin): The big tactical man hiding up the Sun clan’s sleeve. He really fucking hates Zhuge Liang because Zhou Yu is clearly dumber and can’t stop proving just how easy he is to outsmart. Good as a second banana tactician, but he would never allow that. Smokes weed or surfs in his spare time, and microdoses estrogen.

![Zhou Yu](./images/57_zhou_yu.png)

-   Lü Meng (Ziming): A general guy or something under Sun Whateverhisnameis. Starts off kinda incompetent, but studies up later on and does a little bit of kickass military strategy. Probably used to smoke weed or surf with Sun Ce, and totally smokes weed or surfs with Zhou Yu.

![Lü Meng](./images/58_lu_meng.png)

-   Da Qiao: Of the Two Qiao Sisters, she is the Older Qiao. These two sisters are supposed to be so pretty they’re famous, a feat that has never been accomplished before or since in the history of humanity. Married to Sun Ce, but that won’t stop her.

![Da Qiao](./images/59_da_qiao.png)

-   Xiao Qiao: The Younger Qiao. Married to Zhou Yu, and he hasn’t shut up about her since. Zhuge Liang once suggested to Zhou Yu that he hand both sisters to Cao Cao and surrender, and Zhou Yu still flicks his bean to the thought to this day.

![Xiao Qiao](./images/60_xiao_qiao.png)

**Cao Wei**

The Microsoft of the Three Kingdoms era – complete with a policy of [embrace, extend, and extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish). They basically win in the end, which explains all Chinese history from there on out. You can try to surrender, but if Cao Cao wants your blood, he’s going to get it anyway.

-   Cao Cao (Mengde): TSAO TSAO, not COW COW! Just telling you before he gets angry. Cao Cao will do anything for power, and he thrives in this chaotic era. He’s pretty smart, but lets his love for Guan Yu get in the way sometimes. Holds ambition deep in his heart like a minimum wage shift leader.

![Cao Cao](./images/61_cao_cao.png)

Don’t worry, more of the Cao guys are coming.

**Other**

Besides the actual Three Kingdoms people, there are some other relevantly irrelevant fuckers who sometimes pop up in the story.

-   Red Hare: Yeah, it’s just a horse, but it’s going in the character cast list. Red Hare is taken by Cao Cao after Lü Bu does really dumb shit to get killed off, and Cao Cao later gifts it to Guan Yu. If it weren’t for Guan Yu, we wouldn’t be talking about this fucking horse. Where is Red Hare now, anyway?

![Red Hare, Not Looking Particularly Red](./images/62_red_hare_not_looking_particularly_red.png)

-   Yuan Shao (Benchu): I’m not even going to dignify this guy with a proper entry.

![Yuan Shao](./images/63_yuan_shao.png)

-   Yuan Shu (Gonglu): No.

![Yuan Shu](./images/64_yuan_shu.png)

------------------------------------------------------------------------

### An Interview With the Author

**Q:** wha

**A:** wahh

**Q:** Why?

**A:** idk

**Q:** This would have been funnier if you didn’t veer away from using actual MapleStory assets.

**A:** Maybe yeah.

Send in your questions for next time, folks! If not, I’ll just make up more.

Bai, nee\~
