# Guan Yunchang’s World Tour Diary

![Chapter Banner](./images/00_chapter_banner.png)

------------------------------------------------------------------------

## Chapter 1: The Oath of the Mushroom Garden

### False Starts, Disorganized Brains

Welcome to my first attempt at a Maple journal! I’ve wanted to do this sort of thing for a long time, but never knew where to start. You may know my perma-Beginner, **`Taima`**, or any of my \[*insert exaggerated number here*\] characters, most of which both odd jobs AND are members of the guild, [**Oddjobs**](https://oddjobs.codeberg.page/). I can be pretty disorganized and jump from character to character, but here and now I’m hoping to commit to a new challenge!

Over the past several months, I’ve been working on and off creating some resources for a World Tour-themed area-lock challenge, very creatively called the [**World Tour Challenge**](https://codeberg.org/Taima/world_tour). The idea is to enter Spinel’s World Tour at **level 10**, and then (almost) never leave. This challenge includes the combined areas of **Japan, Shanghai, Shaolin Temple, and Thailand**. It does *not* include **Malaysia, Masteria, Singapore, or Taiwan** – for several reasons which I won’t list here yet. That’s not to say that these areas can’t later be added into this version of **“World Tour Lock”** that I’ve concocted.

For now, we are going to keep the challenge simple and interconnected within the Maple Travel Agency’s tour offerings. Much of the content cut out from this challenge may make no sense to others, and there are a lot of content tidbits forgotten about or not fully thought up in this version of **World Tour Lock**. I readily admit that more changes to the rules are needed. Please, give me your suggestions and criticism.

For a look at another version of a **World Tour Challenge** completely unaffiliated with this one, check out [**Game Challenge: World Tour™**](https://mapleroyals.com/forum/threads/game-challenge-world-tour%E2%84%A2.209996/) over on [**MapleRoyals**](https://zombo.com/)! It’s seriously cool to see another group start up this challenge too. Their rules for starting the **World Tour Challenge** seem a bit more forgiving, and *probably make more sense*. But here on **MapleLegends**, we are all about *not making sense*, celebrating Valentine’s Day on February 20th, Anniversary any time from July to December (the server was started on [April 1st](https://en.wikipedia.org/wiki/April_Fools'_Day) for a reason), and Christmas fucking never I guess. And that’s just how we love it.

I’ve had a couple false starts with this challenge; the longest lived attempt was a perma-Thief by the name of **`tousouchuu`**. I noticed pretty quickly that I just really, really don’t want to HP wash as hard as I was trying to! She was having a hard time killing lower level Floating Market and Mushroom Shrine enemies with painfully slow increases in damage output.

![Sad tousouchuu](./images/01_sad_tousouchuu.png)

Instead of pushing through the pain, I quit her for a few months. Now I’ve decided to return to the World Tour, bringing back a legendary warrior some may already know of…

------------------------------------------------------------------------

### The Lady of the Magnificent Beard

Some of you may be familiar with [**Guan Yu**](https://en.wikipedia.org/wiki/Guan_Yu), [style name](https://en.wikipedia.org/wiki/Courtesy_name) **Yunchang** (? CE〜220 CE), a legendary and deified historical figure known for his honorable nature and exploits during the fall of the [Han dynasty](https://en.wikipedia.org/wiki/Han_dynasty) of China.

![Original Guan Yu](./images/02_original_guan_yu.png)

I won’t pretend to actually know much of anything about Chinese history, but as a [Romance of the Three Kingdoms](https://en.wikipedia.org/wiki/Romance_of_the_Three_Kingdoms) fan, I’ve always found Guan Yu’s fictionalized story and depictions in media to be strikingly cool. Given that many “legendary” (*probably* not real at all) items from this time period exist within MapleStory and can be found within the World Tour, I’ve decided to create my new character project based off of the *Lord of the Magnificent Beard* himself.

To start with the **World Tour Challenge**, **`Yunchang`** will be avoiding quests on Maple Island, instead trying to gain EXP and Mesos for Spinel’s travel fare when the time comes.

And thus, the humble beginnings…

------------------------------------------------------------------------

### The Oath of the Mushroom Garden

Looking constipated and unhappy to be brought forth into an unfamiliar plane, our rather beardless hero sets off into the Maple World!

![Constipated Yunchang](./images/03_constipated_yunchang.png)

**(Constipation Meter: Medium)**

![Poopoo Bar 47 Percent](./images/04_poopoo_bar_47_percent.png)

Yunchang gets her first taste of blood on helpless Tutorial mobs.

![Training Camp](./images/05_training_camp.png)

Yunchang gets many Snail card drops… that she cannot pick up. There will be a lot of unwanted cards coming up.

![Too Many Snail Cards](./images/06_too_many_snail_cards.png)

With the Snail population successfully culled, Yunchang moves deeper into the forest. Spores drift through the air as Yunchang settles down for a brief respite.

As her vision fades and her head becomes cloudy, she finds herself transported to an [old memory](https://en.wikipedia.org/wiki/Oath_of_the_Peach_Garden), as Lord [Liu Bei](https://en.wikipedia.org/wiki/Liu_Bei) spoke thusly:

> O Heaven above and Earth below,
>
> O Sun, O Moon, and all creation:
>
> Today, we – Liu Bei, Guan Yu, and [Zhang Fei](https://en.wikipedia.org/wiki/Zhang_Fei) –
>
> swear brotherhood in this Mushroom Garden.
>
> We will grind together as one,
>
> sharing weal and woe for the rest of our lives.
>
> We swear to serve the server and save the playerbase.
>
> We ask not for the same day of character creation,
>
> but we hope to quit on the same day.
>
> May Kimberly bear witness to our utmost and immutable sincerity.

![Oath of the Mushroom Garden](./images/07_oath_of_the_mushroom_garden.png)

That day, the foundation of Maple Island itself shook at the sincerity of our hero in her hallucinatory state.

Sadly, Yunchang will never obtain Pio’s “The Relaxer” – she’ll have to make due with sitting at local benches.

Yunchang realizes that she’ll need lots of Mesos to support herself in this doggy dog world. The residents of Maple Island assure her that the crates in town have free Mesos and potions, and will re-appear infinitely if broken. She collects some dosh for her travel fare off Maple Island. During the search, she also chances upon some **NX cash**!

![Breaking Wooden Boxes and NX Cash](./images/08_breaking_wooden_boxes_and_nx_cash.png)

Yunchang avoids death by Mush Pit. Truly, this place is hellish.

![Avoiding Death by Mush Pit](./images/09_avoiding_death_by_mush_pit.png)

Yunchang sells her stash of respawning wooden crate stuff. Finally, this seems like enough Mesos to live on for a few weeks. Let’s get off this stinky island! Away we go!

![Leaving Maple Island](./images/10_leaving_maple_island.png)

------------------------------------------------------------------------

### A Fresh Start?

Feeling startled by the loud sound and glow of this weird fucking lightbulb thing, she avoids the Monster Researcher desperately trying to call for her attention. Yunchang has street smarts, and knows not to make direct eye contact with anyone who heckles new arrivals fresh off the boat.

![Avoiding the Monster Book Ring](./images/11_avoiding_the_monster_book_ring.png)

Yunchang finds herself on yet another hostile island. A land of *grey crags…*

![Grey Crags](./images/12_grey_crags.png)

*archtrees…*

![Archtrees](./images/13_archtrees.png)

*and everlasting snails.*

![Everlasting Snails](./images/14_everlasting_snails.png)

She clears the monsters in her way, carving a path to the next town.

![Path to Henesys](./images/15_path_to_henesys.png)

This strange woman seems to appear in every town. Yunchang senses something supernatural about her and thinks she’s kind of cute, so decides to chat her up for a moment. It appears that she can take Yunchang on a trip back to the *real world*.

![Supernatural Woman](./images/16_supernatural_woman.png)

But before she can take the ship, Yunchang must do something about her worsening bowel cramps.

![Relaxing at the Park](./images/17_relaxing_at_the_park.png)

Yunchang finds a peaceful park, and gathers her strength for the tough road ahead. She decides to meet with some of the masters in the area, hoping to get her bearings. She meets with a wiseman in the treetop village.

![A Meeting with the Wiseman](./images/18_a_meeting_with_the_wiseman.png)

…But he considers Yunchang too unintelligent to be of any use, and points her in the direction of a local master of the [Art of War](https://en.wikipedia.org/wiki/The_Art_of_War).

Yunchang apologizes for her lack of talent. Performing a deep prostration, she is welcomed into the service of **Dances with Balrog**. *Haaaaaap!*

![In Service of Dances with Balrog](./images/19_in_service_of_dances_with_balrog.png)

Yunchang is given a cheap “sword” with the [Fisher-Price](https://en.wikipedia.org/wiki/Fisher-Price) tag still attached, and is told to cut wood from the living, growling Stumps.

![Fisher-Price Beginner Sword](./images/20_fisher_price_beginner_sword.png)

In dismay at the lack of useful instruction, she takes her leave, trudging into the wilderness until she comes across a city bellowing with polluted fumes.

After drowning her sorrows in liquor and gambling at the Fusion Jazz Bar, Yunchang is able to relieve herself in the unisex bathroom. Given her mental state, Yunchang cares little about the lack of privacy. At last, the bowel cramps have subsided.

![Sweet, Sweet Relief](./images/21_sweet_sweet_relief.png)

Shocked by the screams of protest from the **Drug Lord** beneath the squat toilet, she realizes she’s got to get out of town, and *fast*. Yunchang approaches the supernatural woman who seems to be everywhere at once. Gathering her remaining savings, she asks the strange woman, whose street name is “Spinel,” for the cheapest fare out of town.

Yunchang at this point is so destitute that she must pay with every item she owns, including the clothes on her back. She is permitted to keep only her Fisher-Price plastic sword (for ages 3 and up). She looks no different from the common beggars hanging around the sewers.

![Sayonara Victoria Island!](./images/22_sayonara_victoria_island.png)

While relieved to no longer be in Maple World, Yunchang can’t help but feel she’s made a mistake. Peering at this ramshackle village on the water and cursing herself for her bad decisions, she soldiers on, beginning her new life.

![Fear and Loathing in Thailand](./images/23_fear_and_loathing_in_thailand.png)

*Is Yunchang capable of surviving and building a life from scratch? Will she ever find her brothers, Liu Bei and Zhang Fei, and restore the Han Dynasty to its former glory? How can she stop her intestinal distress from coming back to haunt her?*

Find out next time on **GUAN YUNCHANG’S WORLD TOUR DIARY!**

------------------------------------------------------------------------

### Yunchang’s List of Restrictions (エロイ) \[Yunchang’s LORE\]

A little more on Yunchang’s **restrictions** before we wrap this one up!

These restrictions will be followed in addition to all those present in the [**World Tour Challenge Rules**](https://codeberg.org/Taima/world_tour#rules). This is all subject to change, if that makes the challenge more interesting or fun. We’ll keep you up to date if they *DO* change.

-   **Ironman**: Yunchang may not trade with any other character, transfer items from any other character, pick up items owned by any other character, or party with any other character.
-   **Slow Travel**: Yunchang must pick one area of Spinel’s World Tour in which she must stay for a real time period of three days. This wait period begins as soon as she enters a World Tour area. Each server reset (00:00 server time) subtracts one whole day from the wait period, regardless of if Yunchang has logged in or not. If an exception requires leaving the World Tour, even briefly, she may choose any area to return to. A fresh 3 day wait period begins anew as soon as she re-enters a World Tour area. Spinel offers four distinct areas of travel, which are Japan, Shanghai, Shaolin Temple, and Thailand. Spinel offers two Thailand destinations, but these areas are linked and are thus counted together as “Thailand.”
-   **No Vote**: Yunchang may not receive vote NX on her MapleLegends account, not from red Kimmy or blue Kimmy. NX may only be obtained through monster drops or (much less likely) event rewards, if an exception allows Yunchang to participate.
-   **No FM/CS/Channel Hop Abuse**: Yunchang must walk to a Free Market entrance in town to access the FM, and may not “abuse” Free Market, Cash Shop, or channel warping for faster travel. While FM/CS/Channel Hop “abuse” is hard to define, the purpose is to preserve the danger of maps that take long walks to access, make grinding as a slow Warrior just a bit harder in large maps, and eliminates the cheap feeling of “needing” to warp past maps to travel faster. Yunchang is permitted to AFK in the Cash Shop at any time.
-   [**No Fap**](https://old.reddit.com/r/NoFap/): Yunchang may not pleasure herself via masturbation, whether alone or in a party. This will increase her T levels and sperm count, allowing for stronger polearm swings and high fertility. For Yunchang, this is a most daunting restriction indeed.

Does this all seem too strict? Ironman gives me the most pause, though I’d like to try out the toughest restrictions I can think of! If it’s too much, these can be reverted later.

These restrictions are meant to limit Yunchang’s progression to only what she can achieve based on her own abilities as a warrior. The Slow Travel restriction simulates the “guided tour” aspect of the Maple Travel Agency, forcing long stays in each zone. This requires careful decision making without simply hopping between areas. My philosophy is that it’s simple to relax restrictions as time goes on, but impossible to impose tougher restrictions post-character creation (at least, not in good faith).

My hope is that these additional rules will add more depth to the World Tour theme and more nuance to the challenge, rather than feeling like *needlessly annoying restrictions.* Only time will tell.

*Bai, nee\~*
